import { Component, OnInit } from '@angular/core';
import { Message, MessageType } from '@app/models/message';
import { MessageService } from '@app/services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  messages: Message[] = [];

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.getAlert().subscribe((message: Message) => {
      if (!message) {
        // clear alerts when an empty alert is received
        this.messages = [];
        return;
      }
      // add alert to array
      this.messages.push(message);
    });
  }

  removeAlert(message: Message) {
    this.messages = this.messages.filter(x => x !== message);
  }

  cssClass(message: Message) {
    if (!message) {
        return;
    }

    // return css class based on alert type
    switch (message.type) {
      case MessageType.Success:
        return 'alert-success';
      case MessageType.Error:
        return 'alert-danger';
      case MessageType.Info:
        return 'alert-info';
      case MessageType.Warning:
        return 'alert-warning';
    }
  }

}
