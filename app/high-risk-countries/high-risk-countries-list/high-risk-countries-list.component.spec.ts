import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighRiskCountriesListComponent } from './high-risk-countries-list.component';

describe('HighRiskCountriesListComponent', () => {
  let component: HighRiskCountriesListComponent;
  let fixture: ComponentFixture<HighRiskCountriesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighRiskCountriesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighRiskCountriesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
