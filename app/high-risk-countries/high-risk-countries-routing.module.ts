import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HighRiskCountriesListComponent } from './high-risk-countries-list/high-risk-countries-list.component';

const routes: Routes = [
  {
    path: '',
    component: HighRiskCountriesListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HighRiskCountriesRoutingModule { }
