import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HighRiskCountriesRoutingModule } from './high-risk-countries-routing.module';
import { HighRiskCountriesListComponent } from './high-risk-countries-list/high-risk-countries-list.component';

@NgModule({
  imports: [
    CommonModule,
    HighRiskCountriesRoutingModule
  ],
  declarations: [HighRiskCountriesListComponent]
})
export class HighRiskCountriesModule { }
