import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingsiteListComponent } from './pendingsite/pendingsite/pendingsite-list/pendingsite-list.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from './guards/auth.guard';
import { UserRole } from './models/user-role';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'supplier',
    pathMatch: 'full'
  },
  {
    path: 'supplier',
    loadChildren: './supplier/supplier.module#SupplierModule',
    // canActivate: [AuthGuard],
    // data: { roles: [UserRole.Supplier, UserRole.TradingUser, UserRole.OneTimeLogin] }
  },
  {
    path: 'pendingsite',
    loadChildren: './pendingsite/pendingsite.module#PendingsiteModule',
    canActivate: [AuthGuard],
    data: { roles: [UserRole.TradingUser] }
  },
  {
    path: 'mergesupplier',
    loadChildren: './merge-supplier/merge-supplier.module#MergeSupplierModule',
    canActivate: [AuthGuard],
    data: { roles: [UserRole.TradingUser] }
  },
  {
    path: 'highriskcountries',
    loadChildren: './high-risk-countries/high-risk-countries.module#HighRiskCountriesModule',
    canActivate: [AuthGuard],
    data: { roles: [UserRole.TradingUser] }
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canActivate: [AuthGuard],
    data: { roles: [UserRole.TradingUser] }
  },
  {
    path: 'error',
    component: ErrorPageComponent
  },
  {
    path: '**',
    component: ErrorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
