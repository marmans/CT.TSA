import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignDocumentBarComponent } from './sign-document-bar.component';

describe('SignDocumentBarComponent', () => {
  let component: SignDocumentBarComponent;
  let fixture: ComponentFixture<SignDocumentBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignDocumentBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignDocumentBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
