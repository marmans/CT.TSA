import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sign-document-bar',
  templateUrl: './sign-document-bar.component.html',
  styleUrls: ['./sign-document-bar.component.scss']
})
export class SignDocumentBarComponent implements OnInit {
  @Input() countOfSelectedDocuments = 0;
  @Output() onSignDocumentsClick = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  signDocuments() {
    this.onSignDocumentsClick.emit(true);
  }
}
