import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocunoteDocumentService } from '@app/services/docunote-document.service';
import { DocumentStatus, DocunoteDocument } from '@app/models/document';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IdName } from '@app/models/idName';
import { UserRole } from '@app/models/user-role';
import { ControlType } from '@app/models/ui';
import { AuthorizationService } from '@app/services/authorization.service';
import { MessageService } from '@app/services/message.service';

@Component({
  selector: 'app-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.scss'],
})
export class DocumentsListComponent implements OnInit {
  items = [];
  supplierId: number;
  loading: boolean;
  docListForm: FormGroup;
  selectedDocuments: DocunoteDocument[] = [];
  countOfSelectedDocuments = 0;
  docStatuses: IdName[] = [];
  subscriptions = new Subscription();
  allowToSign: boolean;
  controlTypeEnum = ControlType;

  infoMessage = `<h4>Information message</h4>
  <p>
  Only persons with the types "Main contact", "Contact in quality matters",
   "Person authorised to sign FPA" and "Person authorised to sign PPL" are able to sign documents on this page.
   All other person types are not able to sign documents. One person can have multiple types attached.</p>`;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private documentService: DocunoteDocumentService,
    private fb: FormBuilder,
    private authService: AuthorizationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.messageService.info(this.infoMessage);
    this.getCurrentUser();
    this.initDocStatuses();
    this.getItems();
    this.detectSelectedChanges();
  }
  getCurrentUser() {
    const user = this.authService.getCurrentUser();
    this.subscriptions.add(user.subscribe(usr => {
      this.allowToSign = (usr.Role === UserRole.SupplierSign);
    }));
  }
  initForm() {
    this.docListForm = this.fb.group({
      Documents: this.fb.array([]),
    });
  }
  initDocStatuses() {
    this.docStatuses.push({ Id: DocumentStatus.ToBeSigned, Name: 'To be signed' });
    this.docStatuses.push({ Id: DocumentStatus.Signed, Name: 'Signed' });
  }
  get docs() {
    return this.docListForm.get('Documents') as FormArray;
  }

  getItems() {
    this.supplierId = +this.route.snapshot.paramMap.get('id');
    this.countOfSelectedDocuments = 0;
    this.initForm();
    this.loading = true;
    this.documentService.getDocumentsBySupplierId(this.supplierId).subscribe(res => {
      res.result.forEach(el => {
        this.docs.push(this.getDocRow());
      });
      this.docListForm.patchValue({ Documents: res.result });
      this.loading = false;
    });

  }

  getDocRow() {
    return this.fb.group({
      DocumentId: [''],
      DocumentVersionId: [''],
      Name: [''],
      Url: [''],
      Status: [],
      ContactPersonName: [''],
      SignDate: [''],
      Selected: [false]
    });
  }
  getStatusText(value: number) {
    const f = this.docStatuses.filter(x => x.Id === value);
    return (f && f.length > 0) ? f[0].Name : '';
  }

  detectSelectedChanges() {
    this.docListForm.valueChanges.subscribe(res => {
      const docs: DocunoteDocument[] = res.Documents;
      if (docs && docs.length > 0) {
        this.selectedDocuments = docs.filter(el => el.Selected);
        this.countOfSelectedDocuments = this.selectedDocuments.length;
      }
    });
  }

  signDocuments() {
    const moreThan1 = this.selectedDocuments.length > 1;
    const doc1 = moreThan1 ? 'these documents' : 'this document';
    const doc2 = moreThan1 ? 'Documents were' : 'Document was';
    const doc3 = moreThan1 ? 'Documents' : 'Document';
    const listOfDocs = this.selectedDocuments.map(x => ' • ' + x.Name).join('\r\n');
    const messageConfirm = `Signing of ${doc1} is final and legally binding and can’t be undone. Are you sure you want to proceed?

${doc3} that will be signed:
${listOfDocs}`;
    const messageOk = `${doc2} successfully signed.`;
    if (confirm(messageConfirm)) {
      this.documentService.signDocuments(this.supplierId, this.selectedDocuments).subscribe(res => {
        if (res.result) {
          this.getItems();
          alert(messageOk);
        }
      });
    }
  }
}
