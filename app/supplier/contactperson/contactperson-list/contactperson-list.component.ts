import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactPersonListItem, ContactPersonFilter } from '@app/models/contact-person';
import { ContactPerson, PersonSalutation } from '@app/models/contact-person';
import { ContactPersonService } from '@app/services/contactperson.service';

@Component({
  selector: 'app-contactperson-list',
  templateUrl: './contactperson-list.component.html',
  styleUrls: ['./contactperson-list.component.scss']
})
export class ContactPersonListComponent implements OnInit {
  supplierId: number;
  items: ContactPersonListItem[] = [];
  loading: boolean;
  filterParams: ContactPersonFilter = new ContactPersonFilter();
  total: number;

  constructor(private contactPersonService: ContactPersonService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.supplierId = +this.route.snapshot.paramMap.get('id');
    this.getContactPersons();
  }

  getContactPersons(): void {
    this.filterParams.SupplierId = this.supplierId;
    this.loading = true;
    this.contactPersonService.getContactPersonList(this.filterParams)
      .subscribe(items => {
        this.items = items.result;
      },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      );
  }

  getPersonSalutationText(value: number) {
    return PersonSalutation[value];
  }

  pageChanged(page: number) {
    this.filterParams.Page = page;
    this.getContactPersons();
  }

  filterChanged(filter) {
    this.getContactPersons();
  }
}
