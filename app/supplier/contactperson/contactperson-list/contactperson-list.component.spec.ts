import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonListComponent } from './contactperson-list.component';

describe('ContactPersonListComponent', () => {
  let component: ContactPersonListComponent;
  let fixture: ComponentFixture<ContactPersonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPersonListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
