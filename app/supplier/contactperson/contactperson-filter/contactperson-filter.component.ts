import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ContactPersonFilter } from '@app/models/contact-person';

@Component({
  selector: 'app-contactperson-filter',
  templateUrl: './contactperson-filter.component.html',
  styleUrls: ['./contactperson-filter.component.scss']
})
export class ContactPersonFilterComponent implements OnInit {
  @Input() filter: ContactPersonFilter = new ContactPersonFilter();
  @Output() filterChanged = new EventEmitter<ContactPersonFilter>();
  constructor() { }

  ngOnInit() {
  }

  onChange() {
    this.filterChanged.emit(this.filter);
  }
}
