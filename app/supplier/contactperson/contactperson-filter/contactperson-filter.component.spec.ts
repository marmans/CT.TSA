import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonFilterComponent } from './contactperson-filter.component';

describe('ContactPersonFilterComponent', () => {
  let component: ContactPersonFilterComponent;
  let fixture: ComponentFixture<ContactPersonFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPersonFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
