import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ControlType, NameValue, TextboxType } from '@app/models/ui';
import { IdName } from '@app/models/idName';
import { ContactPerson, PersonSalutation } from '@app/models/contact-person';
import { Language } from '@app/models/language';
import { UserRole } from '@app/models/user-role';
import { ContactPersonService } from '@app/services/contactperson.service';
import { MasterDataService } from '@app/services/masterdata.service';
import { AuthorizationService } from '@app/services/authorization.service';
import { OnetimeloginService } from '@app/services/onetimelogin.service';
import { MessageService } from '@app/services/message.service';
import { Utils } from '@app/services/utils';

@Component({
  selector: 'app-contactperson-details',
  templateUrl: './contactperson-details.component.html',
  styleUrls: ['./contactperson-details.component.scss']
})
export class ContactPersonDetailsComponent implements OnInit, OnDestroy {
  canChangeContactHasLogin: boolean;
  editMode = false;
  contactPersonForm: FormGroup;
  controlTypeEnum = ControlType;
  personSalutations: IdName[] = [];
  allPersonTypes: IdName[] = [];
  languages: Language[] = [];
  isNew: boolean;
  loading: boolean;
  subscriptions = new Subscription();
  textboxType = TextboxType;
  isTradingUser: boolean;
  thisPersonIsCurrentUser: boolean;

  personTypeText = `Persons with the types "Main Contact",
   "Contact in quality matters", "Person authorised to sign FPA"
    and "Person authorised to sign PPL"
     will be able to legally
     binding sign documents electronically. Other types will not.
     One person can have multiple types attached.`;
  gdprText = `<h4>Information of Coop Tradings processing of the Suppliers employees personal data</h4>
  <p>
  Coop Trading process the Supplier and the Supplier personnel personal data (contact information)
   during the term of the contractual relationship and share this information
   with Coop Sverige AB - Coop Norge SA - Coop Danmark A/S – Suomen
   Osuuskauppojen Keskuskunta (SOK).</p>`;

  constructor(
    private contactPersonService: ContactPersonService,
    private masterDataService: MasterDataService,
    private route: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder,
    private authService: AuthorizationService,
    private oneTimeService: OnetimeloginService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.messageService.info(this.gdprText);
    this.getPersonSalutations();
    this.initContactPersonForm();
    this.getContactPerson();
    this.editMode = (this.route.snapshot.routeConfig.path === 'edit');
    this.getCurrentUser();
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
  getCurrentUser() {
    const user = this.authService.getCurrentUser();
    this.subscriptions.add(user.subscribe(usr => {
      if (usr.Role === UserRole.TradingUser) {
        this.isTradingUser = true;
      } else {
        if (usr.PersonId === +this.route.snapshot.paramMap.get('cpid')) {
          this.thisPersonIsCurrentUser = true;
        }
        if (usr.Role === UserRole.OneTimeLogin) {
          this.subscriptions.add(this.oneTimeService.getOneTimeLoginStatus(usr.PersonId)
            .subscribe(res => {
              const status = res.result;
              if (status.IsPersonOneTime || status.IsSupplierOneTime) {
                const message = Utils.getMessageForOneTimeLoginStatus(status.IsPersonOneTime, status.IsSupplierOneTime);
                this.messageService.info(message);
              }
            }));
        }
      }
    }));
  }

  getPersonSalutations() {
    for (const enumMember in PersonSalutation) {
      if (PersonSalutation.hasOwnProperty(enumMember)) {
        const isValueProperty = parseInt(enumMember, 10) >= 0;
        if (isValueProperty) {
          this.personSalutations.push(new IdName(
            parseInt(enumMember, 10),
            PersonSalutation[enumMember]
          ));
        }
      }
    }
  }

  initContactPersonForm() {
    this.contactPersonForm = this.fb.group({
      Id: [''],
      CompanyId: [],
      PersonTypeIds: [],
      PersonSalutationId: [],
      TitleName: [''],
      FirstName: ['', [Validators.required]],
      LastName: ['', [Validators.required]],
      PhoneNumberText: ['', [Validators.required]],
      MobileNumberText: [''],
      FaxNumberText: [''],
      Email: ['', [Validators.required]],
      PrimaryLanguageCode: [''],
      ModifiedDate: [''],
      ModifiedByText: [''],
      ContactHasLogin: [''],
      LoginName: [''],
      IsDisabled: [false]
    });
  }

  getContactPerson() {
    const cpid = +this.route.snapshot.paramMap.get('cpid');
    const supplierId = +this.route.snapshot.paramMap.get('id');
    this.contactPersonForm.get('CompanyId').setValue(supplierId);
    this.isNew = (cpid === 0);
    this.loading = true;
    if (this.isNew) {
      const newFormJoinRequest$ = forkJoin(
        this.contactPersonService.getPersonTypes(),
        this.masterDataService.getLanguages()
      ).pipe(
        map(([personTypes, languages]) => {
          return { personTypes, languages };
        })
      );
      newFormJoinRequest$.subscribe(res => {
        this.allPersonTypes = res.personTypes.result;
        this.languages = res.languages.result;
        this.id.setValue(0);
        this.contactPersonForm.enable();
        this.canChangeContactHasLogin = this.editMode;
        this.contactHasLogin.setValue(true);
        this.onChanges();
      },
        error => {
          console.log(error);
        },
        () => {
          this.loading = false;
        });
      return;
    }

    this.contactPersonForm.disable();

    const formJoinRequest$ = forkJoin(
      this.contactPersonService.getPersonTypes(),
      this.masterDataService.getLanguages(),
      this.contactPersonService.getContactPersonById(cpid)
    ).pipe(
      map(([personTypes, languages, contactPerson]) => {
        return { personTypes, languages, contactPerson };
      })
    );
    this.loading = true;
    formJoinRequest$.subscribe(res => {
      this.allPersonTypes = res.personTypes.result;
      this.languages = res.languages.result;
      this.contactPersonForm.patchValue(res.contactPerson.result);
      this.contactPersonForm.enable();
      const loginExists = this.login.value !== null && this.login.value !== '';
      this.canChangeContactHasLogin = this.editMode && !loginExists;
      this.contactHasLogin.setValue(loginExists);
      this.onChanges();
    },
      error => {
        console.log(error);
      },
      () => {
        this.loading = false;
      });
  }

  goToEdit() {
    this.router.navigate(['../edit'], { relativeTo: this.route });
  }

  goToView() {
    this.router.navigate(['../view'], { relativeTo: this.route });
  }

  save() {
    if (this.contactPersonForm.valid) {
      this.loading = true;
      this.contactPersonService.saveContactPerson(this.contactPersonForm.value)
        .subscribe(res => {
          this.loading = false;
          if (!res.result) {
            console.log(res.error.message);
            return;
          }
          const validationResult = res.result.ValidationResult;
          if (validationResult.IsValid) {
            this.router.navigate(['../../' + res.result.Id], { relativeTo: this.route });
          }
          validationResult.ValidationErrors.forEach(el => {
            const fc = this.contactPersonForm.get(el.PropertyName);
            if (fc) {
              fc.setErrors({
                serverError: el.ErrorMessage
              });
            }
          });
        });
    }
    return false;
  }

  remove() {
    if (confirm('Are you sure you want to delete the contact person')) {
      if (this.isNew) {
        this.router.navigate(['../../'], { relativeTo: this.route });
      } else {
        this.loading = true;
        this.contactPersonService.removeContactPerson(this.id.value)
          .subscribe(res => {
            if (!res.result) {
              console.log(res.error.message);
              return;
            }
            this.router.navigate(['../../'], { relativeTo: this.route });
          }, error => {
            this.loading = false;
            console.log(error);
          });
      }
    }
  }

  get id() {
    return this.contactPersonForm.get('Id');
  }

  get email() {
    return this.contactPersonForm.get('Email');
  }

  get contactHasLogin() {
    return this.contactPersonForm.get('ContactHasLogin');
  }

  get login() {
    return this.contactPersonForm.get('LoginName');
  }

  onChanges(): void {
    this.email.valueChanges.subscribe(val => {
      if (this.contactHasLogin.value) {
        this.login.setValue(this.email.value);
      }
    });
    this.contactHasLogin.valueChanges.subscribe(() => {
      if (this.contactHasLogin.value) {
        this.login.setValue(this.email.value);
      } else {
        this.login.setValue(null);
      }
    });
  }

}
