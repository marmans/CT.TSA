import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierDefectService } from '@app/services/supplier-defect.service';
import { ControlType, ValueLabel } from '@app/models/ui';
import {
  DefectReportedSource, DefectReceivedSource,
  DefectCategory, DefectFollowUp, DefectClosingRemark, SupplierDefect, SupplierDefectProduct
} from '@app/models/supplier-defect';

@Component({
  selector: 'app-defect-details',
  templateUrl: './defect-details.component.html',
  styleUrls: ['./defect-details.component.scss']
})
export class DefectDetailsComponent implements OnInit {
  editMode = false;
  loading: boolean;
  defectForm: FormGroup;
  controlTypeEnum = ControlType;
  isNew: boolean;
  supplierId: number;
  defectId: number;
  defectReportedSources: DefectReportedSource[] = [];
  defectReceivedSources: DefectReceivedSource[] = [];
  defectCategories: DefectCategory[] = [];
  defectFollowUps: DefectFollowUp[] = [];
  defectClosingRemarks: DefectClosingRemark[] = [];
  gtinsForSelect: ValueLabel[] = [];
  gtinsSelected: number[] = [];

  constructor(private route: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder,
    private defectService: SupplierDefectService) { }

  ngOnInit() {
    this.supplierId = +this.route.snapshot.paramMap.get('id');
    this.defectId = +this.route.snapshot.paramMap.get('sdid');
    this.initDefectForm();
    this.editMode = (this.route.snapshot.routeConfig.path === 'edit');
    this.getData();
  }

  getData() {
    this.defectForm.disable();
    this.loading = true;
    const req = this.defectService.getDataForSupplierDefect(this.defectId);
    req.subscribe(res => {
      this.loading = false;
      if (res.length >= 5) {
        this.defectReportedSources = res[0].result;
        this.defectReceivedSources = res[1].result;
        this.defectCategories = res[2].result;
        this.defectFollowUps = res[3].result;
        this.defectClosingRemarks = res[4].result;

        if (res.length === 6) {
          const defect = res[5].result;
          this.fillForm(defect);
        }
      }
      this.defectForm.enable();
    });
  }

  fillForm(defect: SupplierDefect) {
    const sdp = defect.SupplierDefectProducts;
    if (sdp.length > 0) {
      for (let i = 0; i < sdp.length; i++) {
        this.addProductLine();
        this.gtinsForSelect.push({ value: sdp[i].Gtin, label: String(sdp[i].Gtin) });
        this.gtinsSelected.push(sdp[i].Gtin);
      }
    }
    this.defectForm.patchValue(defect);
  }

  initDefectForm() {
    this.defectForm = this.fb.group({
      Id: [this.defectId],
      SupplierId: [this.supplierId],
      ReportedSource: [null, Validators.required],
      ReportDate: ['', Validators.required],
      DefectReceivedSource: [null, Validators.required],
      CreatedByText: [''],
      DefectCategory: [null, Validators.required],
      DefectFollowUp: [null, Validators.required],
      DefectClosingRemark: [null],
      SupplierDefectProducts: this.fb.array([]),
      ShortTermActions: [''],
      Comments: [''],
      ReferenceArchive: ['']
    });
  }

  get products(): FormArray {
    return this.defectForm.get('SupplierDefectProducts') as FormArray;
  }

  public addProductLine() {
    this.products.push(
      this.getProductLineGroup()
    );
    return false;
  }
  getProductLineGroup() {
    return this.fb.group({
      Id: [0],
      Gtin: [null, [Validators.required]],
      ProductText: [''],
      Brand: [''],
      MarketNames: [[]],
      CategoryName: [''],
      LotNo: [''],
      PackagingDate: [''],
      BestBefore: [''],
      isDk: [false],
      isNo: [false],
      isSe: [false],
      isFi: [false]
    });
  }

  showobj() {
    console.log(this.products.controls);
  }

  goToEdit() {
    this.router.navigate(['../edit'], { relativeTo: this.route });
  }

  goToView() {
    this.router.navigate(['../view'], { relativeTo: this.route });
  }

  save() {
    if (this.defectForm.valid) {
      this.loading = true;
      this.defectService.saveSupplierDefect(this.defectForm.value)
        .subscribe(res => {
          this.loading = false;
          const validationResult = res.result.ValidationResult;
          if (validationResult.IsValid) {
            this.router.navigate(['../../' + res.result.Id], { relativeTo: this.route });
          }
          validationResult.ValidationErrors.forEach(el => {
            const fc = this.defectForm.get(el.PropertyName);
            if (fc) {
              fc.setErrors({
                serverError: el.ErrorMessage
              });
            }
          });
        });
    }
    return false;
  }
  removeProduct(index: number) {
    this.products.removeAt(index);
  }

  onSearchGtin(searchText: string) {
    this.defectService.getSupplierGtinsBySearchText(this.supplierId, searchText)
      .subscribe(gtins => {
        this.gtinsForSelect = gtins.result
          // .filter(x => this.gtinsSelected.indexOf(x) === -1)
          .map(x => <ValueLabel>{ value: x, label: String(x) });
      },
        err => {
          console.log(err);
        }
      );
  }

  onGtinSelect(gtin: number, product: FormGroup) {
    this.defectService.getSupplierDefectProduct(this.supplierId, gtin)
      .subscribe(defectProduct => {
        product.patchValue({
          'ProductText': defectProduct.result.ProductText,
          'Brand': defectProduct.result.Brand,
          'MarketNames': defectProduct.result.MarketNames,
          'CategoryName': defectProduct.result.CategoryName
        });
        this.gtinsSelected.push(gtin);
      },
        err => {
          console.log(err);
        }
      );
  }
}
