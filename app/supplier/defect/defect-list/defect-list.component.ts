import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { SupplierDefectService } from '@app/services/supplier-defect.service';
import { SupplierDefectFilter } from '@app/models/supplier-defect';
import { FilterForListsService } from '@app/services/filter-for-lists.service';

@Component({
  selector: 'app-defect-list',
  templateUrl: './defect-list.component.html',
  styleUrls: ['./defect-list.component.scss']
})
export class DefectListComponent implements OnInit, OnDestroy {
  total: number;
  loading: boolean;
  filterParams = new SupplierDefectFilter();
  items = [];
  supplierId: number;
  defaultFilterParams: SupplierDefectFilter = {
    PageSize: 20,
    Page: 1
  };
  subscriptions = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private defectService: SupplierDefectService,
    private filterForListsService: FilterForListsService,
    ) {
      this.filterForListsService.init<SupplierDefectFilter>(this.defaultFilterParams);
  }

  ngOnInit() {
    this.bindFilterParamsToRouteData();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  getSupplierDefects() {
    this.supplierId = +this.route.snapshot.paramMap.get('id');
    this.filterParams.SupplierId = this.supplierId;
    // this.filterParams.Page = 1;
    // this.filterParams.PageSize = 20;
    this.loading = true;
    this.defectService.getSupplierDefectsList(this.filterParams).subscribe(res => {
      this.items = res.result.data;
      this.total = res.result.TotalCount;
    },
    err => {
      console.log(err);
    },
    () => {
      this.loading = false;
    });
  }

  pageChanged(page: number) {
    this.filterParams.Page = page;
    this.filterChanged(true);
  }

  filterChanged(changed: boolean) {
    this.subscriptions.add(this.filterForListsService.getRouteParamsFromFilterParams<SupplierDefectFilter>(this.filterParams)
      .subscribe((urlParams: any) => {
        this.router.navigate(['supplier/' + this.filterParams.SupplierId + '/defect', urlParams]);
      }, err => {
        console.log(err);
        // this.messageService.error('Error getting filter params from route. ' + err);
      }));
  }

  private bindFilterParamsToRouteData() {
    this.subscriptions.add(this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        this.filterForListsService.getFilterParamsFromRouteParamMap<SupplierDefectFilter>(paramMap)
        .subscribe(filterParams => {
          this.filterParams = filterParams;
          this.getSupplierDefects();
        });
      }));
  }
}
