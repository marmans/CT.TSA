import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierListComponent } from './supplier/supplier-list/supplier-list.component';
import { SupplierDetailsComponent } from './supplier/supplier-details/supplier-details.component';
import { SitesListComponent } from './site/sites-list/sites-list.component';
import { ContactPersonListComponent } from './contactperson/contactperson-list/contactperson-list.component';
import { ContactPersonDetailsComponent } from './contactperson/contactperson-details/contactperson-details.component';
import { DefectListComponent } from './defect/defect-list/defect-list.component';
import { DefectDetailsComponent } from './defect/defect-details/defect-details.component';
import { SitesDetailsComponent } from './site/sites-details/sites-details.component';
import { UserRole } from '@app/models/user-role';
import { AuthGuard } from '@app/guards/auth.guard';
import { DocumentsListComponent } from './document/documents-list/documents-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: SupplierListComponent,
        data: { roles: [UserRole.TradingUser] },
        canActivate: [AuthGuard],
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            component: SupplierDetailsComponent,
            redirectTo: 'view',
          },
          {
            path: 'edit',
            component: SupplierDetailsComponent,
            data: { roles: [UserRole.Supplier, UserRole.SupplierSign, UserRole.TradingUser, UserRole.OneTimeLogin] },
            canActivate: [AuthGuard],
          },
          {
            path: 'view',
            component: SupplierDetailsComponent,
            data: { roles: [UserRole.Supplier, UserRole.SupplierSign, UserRole.TradingUser, UserRole.OneTimeLogin] },
            canActivate: [AuthGuard],
          },
          {
            path: 'site',
            children: [
              {
                path: '',
                component: SitesListComponent,
                data: { roles: [UserRole.Supplier, UserRole.SupplierSign, UserRole.TradingUser] },
                canActivate: [AuthGuard],
              },
              {
                path: ':siteid',
                children: [
                  {
                    path: '',
                    component: SitesDetailsComponent,
                    redirectTo: 'view',
                    canActivate: [AuthGuard],
                  },
                  {
                    path: 'edit',
                    component: SitesDetailsComponent,
                    data: { roles: [UserRole.Supplier, UserRole.SupplierSign, UserRole.TradingUser] },
                    canActivate: [AuthGuard],
                  },
                  {
                    path: 'view',
                    component: SitesDetailsComponent,
                    data: { roles: [UserRole.Supplier, UserRole.SupplierSign, UserRole.TradingUser] },
                    canActivate: [AuthGuard],
                  }
                ]
              }
            ]
          },
          {
            path: 'defect',
            children: [
              {
                path: '',
                component: DefectListComponent,
                data: { roles: [UserRole.TradingUser] },
                canActivate: [AuthGuard],
              },
              {
                path: ':sdid',
                children: [
                  {
                    path: '',
                    component: DefectDetailsComponent,
                    redirectTo: 'view',
                    canActivate: [AuthGuard],
                  },
                  {
                    path: 'view',
                    component: DefectDetailsComponent,
                    data: { roles: [UserRole.TradingUser] },
                    canActivate: [AuthGuard],
                  },
                  {
                    path: 'edit',
                    component: DefectDetailsComponent,
                    data: { roles: [UserRole.TradingUser] },
                    canActivate: [AuthGuard],
                  },
                ]
              }
            ]
          },
          {
            path: 'contactperson',
            children: [
              {
                path: '',
                component: ContactPersonListComponent,
                data: { roles: [UserRole.TradingUser, UserRole.SupplierSign, UserRole.Supplier] },
                canActivate: [AuthGuard],
              },
              {
                path: ':cpid',
                children: [
                  {
                    path: '',
                    component: ContactPersonDetailsComponent,
                    redirectTo: 'view'
                  },
                  {
                    path: 'edit',
                    component: ContactPersonDetailsComponent,
                    canActivate: [AuthGuard],
                    // data: { roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.OneTimeLogin] }
                  },
                  {
                    path: 'view',
                    component: ContactPersonDetailsComponent,
                    canActivate: [AuthGuard],
                    // data: { roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.OneTimeLogin] }
                  }
                ]
              }
            ]
          },
          {
            path: 'document',
            children: [
              {
                path: '',
                component: DocumentsListComponent,
                canActivate: [AuthGuard],
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierRoutingModule { }
