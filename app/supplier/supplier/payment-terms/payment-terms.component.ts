import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ControlType } from '@app/models/ui';

@Component({
  selector: 'app-payment-terms',
  templateUrl: './payment-terms.component.html',
  styleUrls: ['./payment-terms.component.scss']
})
export class PaymentTermsComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() group: FormGroup;
  @Input() paymentTerms: FormArray;

  controlTypeEnum = ControlType;

  constructor() { }

  ngOnInit() {
  }

}
