import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SupplierListFilter, SupplierFilterStatus } from '@app/models/supplier';
import { SupplierService } from '@app/services/supplier.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NameValue, ValueLabel } from '@app/models/ui';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-supplier-filter',
  templateUrl: './supplier-filter.component.html',
  styleUrls: ['./supplier-filter.component.scss']
})
export class SupplierFilterComponent implements OnInit {
  filterValue: SupplierListFilter;
  filterForm: FormGroup;
  pageSizeOptions: NameValue[] = [];
  statusItems: ValueLabel[] = [];
  @Input() filter: SupplierListFilter;
  @Output() filterChanged = new EventEmitter<SupplierListFilter>();

  constructor(private supplierService: SupplierService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.getStatuses();
    this.initForm();
    this.onChange();
  }

  initForm() {
    this.pageSizeOptions = [{ Name: '20', Value: '20' }, { Name: '50', Value: '50' }];
    this.filterForm = this.fb.group({
      SupplierName: [''],
      VAT: [''],
      Statuses: [],
      PageSize: ['20'],
    });

    this.filterForm.patchValue(this.filter);
  }

  onChange() {
    this.filterForm.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(res => {
        this.filterChanged.emit(res);
      });
  }

  getStatuses() {
    this.statusItems = [
      { label: 'Active', value: SupplierFilterStatus.Active },
      { label: 'Approved', value: SupplierFilterStatus.Approved },
      { label: 'Blocked', value: SupplierFilterStatus.Blocked },
      { label: 'Pending', value: SupplierFilterStatus.Pending },
      { label: 'Previous', value: SupplierFilterStatus.Previous }
    ];
  }
}
