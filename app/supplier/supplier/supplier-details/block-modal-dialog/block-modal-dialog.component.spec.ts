import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BlockModalDialogComponent } from './block-modal-dialog.component';

describe('BlockModalDialogComponent', () => {
  let component: BlockModalDialogComponent;
  let fixture: ComponentFixture<BlockModalDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlockModalDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockModalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
