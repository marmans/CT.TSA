import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { ModalDialogComponent } from '@app/shared/ui/modal-dialog/modal-dialog.component';
import { ControlType, TextboxType } from '@app/models/ui';
import { SupplierService } from '@app/services/supplier.service';
import { Supplier } from '@app/models/supplier';

@Component({
  selector: 'block-modal-dialog',
  templateUrl: './block-modal-dialog.component.html',
  styleUrls: ['./block-modal-dialog.component.scss']
})
export class BlockModalDialogComponent implements OnInit, OnDestroy, OnChanges {
  @Input() show: boolean;
  @Output() onHide = new EventEmitter<boolean>();
  @Output() onBlock = new EventEmitter<boolean>();

  blockForm: FormGroup;
  loading: boolean;
  controlTypeEnum = ControlType;
  subscriptions = new Subscription();

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private supplierService: SupplierService
  ) {
  }

  ngOnInit() {
    this.initBlockForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.show) {
      this.setSupplierId();
    }
  }

  initBlockForm() {
    this.blockForm = this.fb.group({
      Id: [],
      DisabledRemark: ['', [Validators.required]]
    });
  }

  setSupplierId() {
    this.blockForm.reset();
    const supplier = new Supplier();
    supplier.Id = +this.route.snapshot.paramMap.get('id');
    this.blockForm.patchValue(supplier);
    this.blockForm.enable();
  }

  block() {
    if (this.blockForm.valid) {
      this.loading = true;
      this.supplierService.blockSupplier(this.blockForm.value)
        .subscribe(res => {
          this.loading = false;
          if (!res.result) {
            console.log(res.error.message);
            return;
          }
          this.hide();
          this.onBlock.emit(true);
        });
    }
    return false;
  }

  hide() {
    this.show = false;
    this.onHide.emit(true);
  }
}
