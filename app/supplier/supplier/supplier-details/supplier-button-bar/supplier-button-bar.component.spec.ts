import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierButtonBarComponent } from './supplier-button-bar.component';

describe('SupplierButtonBarComponent', () => {
  let component: SupplierButtonBarComponent;
  let fixture: ComponentFixture<SupplierButtonBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierButtonBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
