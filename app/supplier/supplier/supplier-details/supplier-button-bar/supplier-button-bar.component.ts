import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-supplier-button-bar',
  templateUrl: './supplier-button-bar.component.html',
  styleUrls: ['./supplier-button-bar.component.scss']
})
export class SupplierButtonBarComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() isTrading: boolean;
  @Input() isSupplierBlocked: boolean;
  @Input() formIsValid: boolean;
  @Output() onEditClicked = new EventEmitter<boolean>();
  @Output() onCancelClicked = new EventEmitter<boolean>();
  @Output() onSaveClicked = new EventEmitter<boolean>();
  @Output() onBlockClicked = new EventEmitter<boolean>();
  @Output() onUnblockClicked = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  editClicked() {
    this.onEditClicked.emit(true);
  }
  cancelClicked() {
    this.onCancelClicked.emit(true);
  }
  saveClicked() {
    this.onSaveClicked.emit(true);
  }
  blockClicked() {
    this.onBlockClicked.emit(true);
  }
  unblockClicked() {
    this.onUnblockClicked.emit(true);
  }
}
