import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { vatValidator } from '@app/shared/custom-validators/vat.validator';
import { ControlType, TextboxType } from '@app/models/ui';
import { UserRole } from '@app/models/user-role';
import { Supplier } from '@app/models/supplier';
import { Utils } from '@app/services/utils';
import { AuthorizationService } from '@app/services/authorization.service';
import { MasterDataService } from '@app/services/masterdata.service';
import { OnetimeloginService } from '@app/services/onetimelogin.service';
import { MessageService } from '@app/services/message.service';
import { SupplierService } from '@app/services/supplier.service';
import { numericValidator } from '@app/shared/custom-validators/numeric.validator';

@Component({
  selector: 'app-supplier-details',
  templateUrl: './supplier-details.component.html',
  styleUrls: ['./supplier-details.component.scss']
})
export class SupplierDetailsComponent implements OnInit, OnDestroy {
  supplier: Supplier = new Supplier();
  showVisitingAddress: boolean;
  editMode = false;
  isBlockModalDialogVisible = false;
  isSupplierBlocked = false;
  supplierForm: FormGroup;
  visitingAddressForm: FormGroup;
  controlTypeEnum = ControlType;
  countries: any[] = [];
  isNew: boolean;
  loading: boolean;
  isTrading: boolean;
  vatNumberEditable = false;
  subscriptions = new Subscription();
  textboxType = TextboxType;
  isPackagingMaterialSupplierText = `Only used if the supplier is not
    a supplier of products but a supplier of packaging materials to other suppliers.`;
  emergencyNumberText = `Telephone number in case of emergency.
     Must be a number that can be reached 24 hours a day for serious situations
      requiring immediate action.`;

  constructor(
    private supplierService: SupplierService,
    private masterDataService: MasterDataService,
    private route: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder,
    private authService: AuthorizationService,
    private oneTimeService: OnetimeloginService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.initSupplierForm();
    this.getCurrentUser();
    this.getSupplier();
    this.editMode = (this.route.snapshot.routeConfig.path === 'edit');
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
  getCurrentUser() {
    const user = this.authService.getCurrentUser();
    this.subscriptions.add(user.subscribe(usr => {
      this.isTrading = usr.Role === UserRole.TradingUser;
      if (usr.Role === UserRole.OneTimeLogin) {
        this.subscriptions.add((this.oneTimeService.getOneTimeLoginStatus(usr.PersonId)
          .subscribe(res => {
            const status = res.result;
            if (status.IsPersonOneTime || status.IsSupplierOneTime) {
              const message = Utils.getMessageForOneTimeLoginStatus(status.IsPersonOneTime, status.IsSupplierOneTime);
              this.messageService.info(message);
            }
          })));
      }
    }));
  }
  initSupplierForm() {
    this.supplierForm = this.fb.group({
      Id: [],
      Name: ['', [Validators.required]],
      CountryIsoCode: [''],
      GlnNumber: [''],
      VAT: ['', [Validators.required, vatValidator()]],
      Status: [],
      Url: [''],
      MainContacts: [],
      CanSendEdiMessageBol: [],
      IsAgent: [],
      IsActiveFaV: [false],
      IsIndirectGaS: [false],
      IsPackagingMaterialsSupplier: [false],
      PrimaryPostalAddress: this.fb.group({
        Id: [0],
        AddressLine1Text: ['', Validators.required],
        ZipCodeTerm: [''],
        CityName: [''],
        CountryIsoCode: ['', Validators.required],
        PhoneNumberText: [''],
        FaxNumberText: [''],
        EmergencyNumberText: ['', [Validators.required]]
      }),
      PaymentTerms: this.fb.array([])
    });
  }
  get primaryPostalAddress() {
    return this.supplierForm.get('PrimaryPostalAddress');
  }
  get primaryVisitingAddress() {
    return this.supplierForm.get('PrimaryVisitingAddress');
  }
  get mainContacts() {
    return this.supplierForm.get('MainContacts');
  }
  get paymentTerms(): FormArray {
    return this.supplierForm.get('PaymentTerms') as FormArray;
  }
  get vat() {
    return this.supplierForm.get('VAT');
  }
  // get vatNumberEditable() {
  //   if (this.isTrading) {
  //     return true;
  //   }
  //   return !this.vat.value;
  // }
  getSupplier() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.isNew = (id === 0);
    this.supplierForm.disable();
    if (this.isNew) {
      const valueNew$ = forkJoin(
        this.masterDataService.getCountries(),
        this.supplierService.getEmptyPaymentTermsList()
      ).pipe(
        map(([countries, paymentTerms]) => {
          return { countries, paymentTerms };
        })
      );
      this.loading = true;
      valueNew$.subscribe(res => {
        this.loading = false;
        this.countries = res.countries.result;
        const emptyPaymentTerms = res.paymentTerms.result;
        for (let i = 0; i < emptyPaymentTerms.length; i++) {
          this.addPaymentTerm();
        }
        this.supplierForm.patchValue({ PaymentTerms: emptyPaymentTerms });
        this.supplierForm.enable();
      });
      return;
    }
    const value$ = forkJoin(
      this.masterDataService.getCountries(),
      this.supplierService.getSupplierWithAddressesById(id)
    ).pipe(
      map(([countries, supplier]) => {
        return { countries, supplier };
      })
    );
    this.loading = true;
    value$.subscribe(res => {
      let supplier: Supplier = res.supplier.result;
      supplier = this.updatePaymentTermsAfterGet(supplier);
      if (supplier.PrimaryVisitingAddress) {
        this.addVisitingAddressForm();
      }
      this.isSupplierBlocked = supplier.Status === 'Blocked';

      if (this.isTrading) {
        this.vatNumberEditable = true;
      } else {
        this.vatNumberEditable = (supplier.VAT === '0');
      }

      this.countries = res.countries.result;
      for (let i = 0; i < supplier.PaymentTerms.length; i++) {
        this.addPaymentTerm();
      }
      this.supplierForm.patchValue(supplier);
      // when Supplier was created using onetimelogin user it sets vat=0
      // so reset it here
      if (this.vat.value === '0') {
        this.vat.setValue(null);
      }
      this.supplierForm.enable();
    },
      error => {
        console.log(error);
      },
      () => {
        this.loading = false;
      });
  }

  addPaymentTerm() {
    this.paymentTerms.push(this.getPaymentTermRow());
  }

  getPaymentTermRow() {
    return this.fb.group({
      NetDays: [null, numericValidator(true)],
      CashDiscount: [null, numericValidator(true)],
      Comment: [''],
      MarketId: [],
      MarketName: [''],
      SupplierCompanyId: []
    });
  }

  goToEdit() {
    this.router.navigate(['../edit'], { relativeTo: this.route });
  }

  goToView() {
    this.router.navigate(['../view'], { relativeTo: this.route });
  }

  showBlockModal() {
    this.isBlockModalDialogVisible = true;
  }

  hideBlockModal() {
    this.isBlockModalDialogVisible = false;
  }

  unblock() {
    const supplierId = +this.route.snapshot.paramMap.get('id');
    this.supplierService.unblockSupplier(supplierId)
      .subscribe(res => {
        this.loading = false;
        if (!res.result) {
          console.log(res.error.message);
          return;
        }
        this.getSupplier();
      });
  }

  updatePaymentTermsAfterGet(supplier: Supplier): Supplier {
    supplier.PaymentTerms.forEach(el => {
      if (el.NetDays) {
        el.NetDays = (el.NetDays + '').replace('.', ',');
      }
      if (el.CashDiscount) {
        el.CashDiscount = (el.CashDiscount + '').replace('.', ',');
      }
    });
    return supplier;
  }

  updatePaymentTermsBeforeSave(supplier: Supplier): Supplier {
    supplier.PaymentTerms.forEach(el => {
      if (el.NetDays) {
        el.NetDays = (el.NetDays + '').replace(',', '.');
      }
      if (el.CashDiscount) {
        el.CashDiscount = (el.CashDiscount + '').replace(',', '.');
      }
    });
    return supplier;
  }

  save() {
    if (this.supplierForm.valid) {
      this.loading = true;
      if (this.isNew) {
        this.supplierForm.patchValue({ Id: 0 });
      }
      const updatedPaymentTerms = this.updatePaymentTermsBeforeSave(this.supplierForm.value);
      this.supplierService.saveSupplier(updatedPaymentTerms)
        .subscribe(res => {
          this.loading = false;
          const result = res.result;
          if (!result) {
            console.log(res.error.message);
            return;
          }
          if (result.ValidationResult.IsValid) {
            this.router.navigate(['../../' + res.result.Id], { relativeTo: this.route });
          }
          result.ValidationResult.ValidationErrors.forEach(el => {
            const fc = this.supplierForm.get(el.PropertyName);
            if (fc) {
              fc.setErrors({
                serverError: el.ErrorMessage
              });
            }
          });
        });
    }
    console.log(this.supplierForm.value);
    return false;
  }

  addVisitingAddressClicked() {
    this.addVisitingAddressForm();
  }

  removeVisitingAddressClicked() {
    this.showVisitingAddress = false;
    this.supplierForm.removeControl('PrimaryVisitingAddress');
  }

  private addVisitingAddressForm() {
    this.supplierForm.addControl('PrimaryVisitingAddress', this.fb.group({
      Id: [0],
      AddressLine1Text: [''],
      PostBoxTerm: [''],
      ZipCodeTerm: [''],
      CityName: [''],
      CountryIsoCode: [''],
      PhoneNumberText: [''],
      MobileNumberText: [''],
      FaxNumberText: [''],
      ModifiedDate: [''],
      ModifiedByText: [''],
      CreatedDate: [''],
      CreatedByText: ['']
    }));
    this.showVisitingAddress = true;
  }
}
