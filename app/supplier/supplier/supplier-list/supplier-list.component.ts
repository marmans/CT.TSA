import { Component, OnInit, OnDestroy } from '@angular/core';
import { SupplierService } from '@app/services/supplier.service';
import { SupplierListItem, SupplierListFilter, SupplierFilterStatus } from '@app/models/supplier';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';
import { FilterForListsService } from '@app/services/filter-for-lists.service';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.scss']
})
export class SupplierListComponent implements OnInit, OnDestroy {
  total: number;
  loading: boolean;
  filterParams = new SupplierListFilter();
  suppliers: SupplierListItem[] = [];
  defaultFilterParams: SupplierListFilter = {
    PageSize: 20,
    Page: 1,
    SupplierName: '',
    VAT: '',
    Statuses: [
      SupplierFilterStatus.Active,
      SupplierFilterStatus.Approved,
      SupplierFilterStatus.Blocked,
      SupplierFilterStatus.Pending,
      SupplierFilterStatus.Previous
    ]
  };
  subscriptions = new Subscription();

  constructor(
    private supplierService: SupplierService,
    private route: ActivatedRoute,
    private router: Router,
    private filterForListsService: FilterForListsService,
  ) {
    this.filterForListsService.init<SupplierListFilter>(this.defaultFilterParams);
  }

  ngOnInit() {
    this.bindFilterParamsToRouteData();
  }

  getSuppliers(): void {
    this.loading = true;
    this.subscriptions.add(
      this.supplierService.getSuppliers(this.filterParams).subscribe(suppliers => {
        this.suppliers = suppliers.result.data;
        this.total = suppliers.result.TotalCount;
      },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      )
    );
  }

  filterChanged(filter: SupplierListFilter) {
    this.filterParams = filter;
    this.subscriptions.add(this.filterForListsService.getRouteParamsFromFilterParams<SupplierListFilter>(this.filterParams)
      .subscribe((urlParams: any) => {
        this.router.navigate(['/supplier', urlParams]);
      }, err => {
        console.log(err);
        // this.messageService.error('Error getting filter params from route. ' + err);
      }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  pageChanged(page: number) {
    this.filterParams.Page = page;
    this.filterChanged(this.filterParams);
  }

  private bindFilterParamsToRouteData() {
    this.subscriptions.add(this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        this.filterForListsService.getFilterParamsFromRouteParamMap<SupplierListFilter>(paramMap).subscribe(filterParams => {
          this.filterParams = filterParams;
          this.getSuppliers();
        });
      }));
  }
}
