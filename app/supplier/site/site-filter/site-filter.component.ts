import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SiteListFilter } from '@app/models/site';

@Component({
  selector: 'app-site-filter',
  templateUrl: './site-filter.component.html',
  styleUrls: ['./site-filter.component.scss']
})
export class SiteFilterComponent implements OnInit {
  @Input() filter = new SiteListFilter();
  @Output() filterChanged = new EventEmitter<SiteListFilter>();
  constructor() { }

  ngOnInit() {
  }

  onChange() {
    this.filterChanged.emit(this.filter);
  }

}
