import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteButtonBarComponent } from './site-button-bar.component';

describe('SiteButtonBarComponent', () => {
  let component: SiteButtonBarComponent;
  let fixture: ComponentFixture<SiteButtonBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteButtonBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
