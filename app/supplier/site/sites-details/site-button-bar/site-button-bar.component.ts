import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-site-button-bar',
  templateUrl: './site-button-bar.component.html',
  styleUrls: ['./site-button-bar.component.scss']
})
export class SiteButtonBarComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() hideDisable: boolean;
  @Input() hideRemove: boolean;
  @Input() formIsValid: boolean;

  @Input() disabledRemove: boolean;
  @Input() disabledRemoveTooltip: string;

  @Output() onEditClicked = new EventEmitter<boolean>();
  @Output() onCancelClicked = new EventEmitter<boolean>();
  @Output() onSaveClicked = new EventEmitter<boolean>();
  @Output() onRemoveClicked = new EventEmitter<boolean>();
  @Output() onDisableClicked = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  editClicked() {
    this.onEditClicked.emit(true);
  }
  cancelClicked() {
    this.onCancelClicked.emit(true);
  }
  saveClicked() {
    this.onSaveClicked.emit(true);
  }
  removeClicked() {
    this.onRemoveClicked.emit(true);
  }
  disableClicked() {
    this.onDisableClicked.emit(true);
  }
}
