import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { conditionalValidator } from '@app/shared/custom-validators/conditional.validator';
import { ControlType, ValueLabel } from '@app/models/ui';
import { Address, AddressTypeEnum, AddressStatus } from '@app/models/site';
import { IdName } from '@app/models/idName';
import { CountryPort } from '@app/models/country';
import { UserInfo } from '@app/models/user-info';
import { UserRole } from '@app/models/user-role';
import { MasterDataService } from '@app/services/masterdata.service';
import { SiteService } from '@app/services/site.service';
import { AuthorizationService } from '@app/services/authorization.service';
import { Utils } from '@app/services/utils';

@Component({
  selector: 'app-sites-details',
  templateUrl: './sites-details.component.html',
  styleUrls: ['./sites-details.component.scss']
})
export class SitesDetailsComponent implements OnInit {
  editMode = false;
  loading: boolean;
  supplierSiteForm: FormGroup;
  controlTypeEnum = ControlType;
  isNew: boolean;
  hasDisabledStatus: boolean;
  countries: any[] = [];
  addressTypes: IdName[] = [];
  addressStatuses: IdName[] = [];
  messageFillZipCode = 'ZipCode is required';
  countryPortTooltip = `Only to be filled in when the INCO term is related to sea freight.
   It is the identification for the local port/harbor used by the supplier/freight forwarder.`;
  authorityApprovalTooltip = `“Health and identification marking” for food products of animal origin according to EU 853/2004.
   For pet food products approval- and/or registration number`;

  removeTooltipText = Utils.getTooltipTextForSiteDelete();
  private unselectedOption: ValueLabel = { value: null, label: '--Not selected--' };
  countryPorts: ValueLabel[] = [];
  isPickupSite: boolean;
  isTradingUser: boolean;

  subscriptions = new Subscription();

  constructor(
    private authorizationService: AuthorizationService,
    private siteService: SiteService,
    private masterDataService: MasterDataService,
    private route: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initSupplierSiteForm();
    this.getSupplierSite();
    this.subscriptions.add(
      this.authorizationService.getCurrentUser()
        .subscribe((user: UserInfo) => {
          this.isTradingUser = user.Role === UserRole.TradingUser;
        })
    );
    this.editMode = (this.route.snapshot.routeConfig.path === 'edit');
  }

  initSupplierSiteForm() {
    this.supplierSiteForm = this.fb.group({
      Id: [''],
      CompanyId: [''],
      AddressStatusId: ['', [conditionalValidator(
        (() => this.isTradingUser),
        Validators.required
      )]],
      AddressTypes: ['', [Validators.required]],
      Name: ['', [Validators.required]],
      AddressLine1Text: ['', [Validators.required]],
      PostBoxTerm: [''],
      ZipCodeTerm: ['', [Validators.required]],
      CityName: [''],
      CountryIsoCode: ['', [Validators.required]],
      CountryPortId: [],
      PhoneNumberText: [''],
      MobileNumberText: [''],
      FaxNumberText: [''],
      AuthorityApprovalNumber: [''],
      ModifiedDate: [''],
      ModifiedByText: [''],
      IsUsedInPpl: [],
      IsUsedInSpecifications: [],
      Description: [''],
    });
  }

  getSupplierSite() {
    const siteid = +this.route.snapshot.paramMap.get('siteid');
    const supplierId = +this.route.snapshot.paramMap.get('id');
    this.supplierSiteForm.get('CompanyId').setValue(supplierId);
    this.isNew = siteid === 0;
    this.supplierSiteForm.disable();
    if (this.isNew) {
      const newFormJoinRequest$ = forkJoin(
        this.masterDataService.getCountries(),
        this.siteService.getAddressTypes(),
        this.siteService.getAddressStatuses()
      ).pipe(
        map(([countries, types, statuses]) => {
          return { countries, types, statuses };
        })
      );
      this.loading = true;
      newFormJoinRequest$.subscribe(res => {
        this.countries = res.countries.result;
        this.addressTypes = res.types.result;
        this.addressStatuses = res.statuses.result;
        this.id.setValue(0);
        this.supplierSiteForm.enable();
        this.onChanges();
      },
        error => {
          console.log(error);
        },
        () => {
          this.loading = false;
        });
      return;
    }
    const formJoinRequest$ = forkJoin(
      this.masterDataService.getCountries(),
      this.siteService.getAddressTypes(),
      this.siteService.getAddressStatuses(),
      this.siteService.getSupplierSite(siteid)
    ).pipe(
      map(([countries, types, statuses, supplierSite]) => {
        return { countries, types, statuses, supplierSite };
      })
    );
    this.loading = true;
    formJoinRequest$.subscribe(res => {
      this.loading = false;
      const supplier: Address = res.supplierSite.result;
      this.countries = res.countries.result;
      this.addressStatuses = res.statuses.result;
      if (supplier.CountryPortId) {
        this.masterDataService.getCountryPort(supplier.CountryPortId)
          .subscribe(data => {
            this.countryPorts = data ? [{ value: data.result.Id, label: this.generateCountryPortNameProperty(data.result) }] : [];
            this.supplierSiteForm.patchValue(supplier);
            setTimeout(() => {
              // thin ice of shitcode. something wrong with select2 component. i'm sorry
              this.addressTypes = res.types.result;
              this.supplierSiteForm.patchValue(supplier);
              this.isPickupSite = this.addressTypesControl.value.indexOf(AddressTypeEnum.Pickup) > -1;
              this.supplierSiteForm.enable();
              this.onChanges();
            }, 1);
          });
      } else {
        this.addressTypes = res.types.result;
        this.supplierSiteForm.patchValue(supplier);
        this.isPickupSite = this.addressTypesControl.value.indexOf(AddressTypeEnum.Pickup) > -1;
        this.hasDisabledStatus = this.supplierSiteForm.get('AddressStatusId').value === AddressStatus.Disabled;
        this.supplierSiteForm.enable();
        this.onChanges();
      }
    },
      error => {
        console.log(error);
      },
      () => {
        this.loading = false;
      });
  }

  goToEdit() {
    this.router.navigate(['../edit'], { relativeTo: this.route });
  }

  goToView() {
    this.router.navigate(['../view'], { relativeTo: this.route });
  }

  save() {
    if (this.supplierSiteForm.valid) {
      this.loading = true;
      this.siteService.saveSupplierSite(this.supplierSiteForm.value)
        .subscribe(res => {
          this.loading = false;
          if (!res.result) {
            console.log(res.error.message);
            return;
          }
          const validationResult = res.result.ValidationResult;
          if (validationResult.IsValid) {
            this.router.navigate(['../../' + res.result.Id], { relativeTo: this.route });
          }
          validationResult.ValidationErrors.forEach(el => {
            const fc = this.supplierSiteForm.get(el.PropertyName);
            if (fc) {
              fc.setErrors({
                serverError: el.ErrorMessage
              });
            }
          });
        });
    }
    return false;
  }

  remove() {
    if (confirm('Are you sure you want to delete the supplier site')) {
      if (this.isNew) {
        this.router.navigate(['../../'], { relativeTo: this.route });
      } else {
        this.siteService.removeSupplierSite(this.id.value)
          .subscribe(res => {
            this.router.navigate(['../../'], { relativeTo: this.route });
          }, error => {
            console.log(error);
          });
      }
    }
  }

  disable() {
    if (confirm('Are you sure you want to disable the supplier site')) {
      this.siteService.disableSupplierSite(this.id.value)
        .subscribe(res => {
          this.getSupplierSite();
        }, error => {
          console.log(error);
        });
    }
  }

  get id() {
    return this.supplierSiteForm.get('Id');
  }

  get addressTypesControl() {
    return this.supplierSiteForm.get('AddressTypes');
  }

  get countryPortIdControl() {
    return this.supplierSiteForm.get('CountryPortId');
  }

  get disableRemove() {
    const ppl = this.supplierSiteForm.get('IsUsedInPpl').value;
    const spec = this.supplierSiteForm.get('IsUsedInSpecifications').value;
    return (ppl || spec);
  }

  onSearchCountryPorts(searchText: string) {
    this.masterDataService.getCountryPortsBySearchText(searchText)
      .subscribe(cp => {
        this.countryPorts = cp.result.map(x => <ValueLabel>{ value: x.Id, label: this.generateCountryPortNameProperty(x) });
        this.countryPorts.unshift(this.unselectedOption);
      },
        err => {
          console.log(err);
        }
      );
  }

  private generateCountryPortNameProperty(countryPort: CountryPort) {
    return `${countryPort.Country.Name}-${countryPort.PortName}`;
  }

  private onChanges(): void {
    this.addressTypesControl.valueChanges.subscribe(val => {
      this.isPickupSite = this.addressTypesControl.value.indexOf(AddressTypeEnum.Pickup) > -1;
      if (!this.isPickupSite) {
        this.countryPortIdControl.setValue(null);
      }
    });
  }

}
