import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';
import { Address, SiteListFilter, AddressStatus } from '@app/models/site';
import { UserInfo } from '@app/models/user-info';
import { UserRole } from '@app/models/user-role';
import { AuthorizationService } from '@app/services/authorization.service';
import { SiteService } from '@app/services/site.service';
import { FilterForListsService } from '@app/services/filter-for-lists.service';
import { Utils } from '@app/services/utils';
import { IdName } from '@app/models/idName';

@Component({
  selector: 'app-sites-list',
  templateUrl: './sites-list.component.html',
  styleUrls: ['./sites-list.component.scss']
})
export class SitesListComponent implements OnInit, OnDestroy {
  supplierId: number;
  total: number;
  loading: boolean;
  filterParams = new SiteListFilter();
  sites: Address[] = [];
  isTrading = false;
  removeTooltipText = Utils.getTooltipTextForSiteDelete();
  subscriptions = new Subscription();
  defaultFilterParams: SiteListFilter = {
    PageSize: '20',
    Page: 1,
    ShowActiveOnly: true
  };
  statuses: IdName[] = [];
  messageFillZipCode = 'ZipCode is required';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authorizationService: AuthorizationService,
    private siteService: SiteService,
    private filterForListsService: FilterForListsService,
  ) {
  }

  ngOnInit() {
    this.initStatuses();
    this.supplierId = +this.route.snapshot.paramMap.get('id');
    this.defaultFilterParams.SupplierId = this.supplierId;
    this.filterForListsService.init<SiteListFilter>(this.defaultFilterParams);
    this.bindFilterParamsToRouteData();

    this.subscriptions.add(
      this.authorizationService.getCurrentUser()
        .subscribe((user: UserInfo) => {
          this.isTrading = user.Role === UserRole.TradingUser;
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  getSuppliersSites(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.filterParams.SupplierId = id;
    this.loading = true;
    this.subscriptions.add(
      this.siteService.getSupplierSiteList(this.filterParams).subscribe(response => {
        this.sites = response.result.data;
        this.total = response.result.TotalCount;
      },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      ));
  }

  filterChanged(changed: boolean) {
    this.subscriptions.add(this.filterForListsService.getRouteParamsFromFilterParams<SiteListFilter>(this.filterParams)
      .subscribe((urlParams: any) => {
        this.router.navigate(['supplier/' + this.filterParams.SupplierId + '/site', urlParams]);
      }, err => {
        console.log(err);
      }));
  }

  pageChanged(page: number) {
    this.filterParams.Page = page;
    this.filterChanged(true);
  }

  remove(id: number) {
    if (confirm('Are you sure you want to delete the supplier site')) {
      this.siteService.removeSupplierSite(id)
        .subscribe(res => {
          this.getSuppliersSites();
        }, error => {
          console.log(error);
        });
    }
  }

  private bindFilterParamsToRouteData() {
    this.subscriptions.add(this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        this.filterForListsService.getFilterParamsFromRouteParamMap<SiteListFilter>(paramMap)
          .subscribe(filterParams => {
            this.filterParams = filterParams;
            this.getSuppliersSites();
          });
      }));
  }

  initStatuses() {
    const s1 = new IdName(AddressStatus.Approved, 'Approved');
    const s2 = new IdName(AddressStatus.ApprovedBySystem, 'Approved by system');
    const s3 = new IdName(AddressStatus.ApprovedWillPerformAudit, 'Approved will perform audit');
    const s4 = new IdName(AddressStatus.ApprovedWithoutAudit, 'Approved without audit');
    const s5 = new IdName(AddressStatus.AwaitingApproval, 'Awaiting approval');
    const s6 = new IdName(AddressStatus.Disabled, 'Disabled');
    const s7 = new IdName(AddressStatus.Rejected, 'Rejected');
    this.statuses = [s1, s2, s3, s4, s5, s6, s7];
  }

  private getStatusText(value) {
    const f = this.statuses.filter(x => x.Id === value);
    return (f && f.length > 0) ? f[0].Name : '';
  }
}
