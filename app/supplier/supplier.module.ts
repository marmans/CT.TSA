import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupplierRoutingModule } from './supplier-routing.module';
import { SupplierListComponent } from './supplier/supplier-list/supplier-list.component';
import { SupplierDetailsComponent } from './supplier/supplier-details/supplier-details.component';
import { SitesListComponent } from './site/sites-list/sites-list.component';
import { SitesDetailsComponent } from './site/sites-details/sites-details.component';
import { SiteButtonBarComponent } from './site/sites-details/site-button-bar/site-button-bar.component';
import { SharedModule } from '../shared/shared.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { SupplierFilterComponent } from './supplier/supplier-filter/supplier-filter.component';
import { SupplierButtonBarComponent } from './supplier/supplier-details/supplier-button-bar/supplier-button-bar.component';
import { BlockModalDialogComponent } from './supplier/supplier-details/block-modal-dialog/block-modal-dialog.component';
import { ContactPersonListComponent } from './contactperson/contactperson-list/contactperson-list.component';
import { ContactPersonDetailsComponent } from './contactperson/contactperson-details/contactperson-details.component';
import { ContactPersonFilterComponent } from './contactperson/contactperson-filter/contactperson-filter.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DefectListComponent } from './defect/defect-list/defect-list.component';
import { DefectDetailsComponent } from './defect/defect-details/defect-details.component';
import { DocumentsListComponent } from './document/documents-list/documents-list.component';
import { PaymentTermsComponent } from './supplier/payment-terms/payment-terms.component';
import { SignDocumentBarComponent } from './document/sign-document-bar/sign-document-bar.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { SiteFilterComponent } from './site/site-filter/site-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPaginationModule,
    SupplierRoutingModule,
    TooltipModule,
  ],
  declarations: [
    SupplierListComponent,
    SupplierDetailsComponent,
    SitesListComponent,
    SitesDetailsComponent,
    SiteButtonBarComponent,
    SupplierFilterComponent,
    SupplierButtonBarComponent,
    BlockModalDialogComponent,
    ContactPersonListComponent,
    ContactPersonDetailsComponent,
    ContactPersonFilterComponent,
    DefectListComponent,
    DefectDetailsComponent,
    DocumentsListComponent,
    SignDocumentBarComponent,
    PaymentTermsComponent,
    SiteFilterComponent,
  ],
  exports: [
    PaymentTermsComponent
  ]
})
export class SupplierModule { }
