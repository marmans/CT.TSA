import { Component, OnInit } from '@angular/core';
import { NavItem } from '../models/navigation';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NavigationService } from '../services/navigation.service';
import { AuthorizationService } from '@app/services/authorization.service';
import { UserRole } from '@app/models/user-role';
import { SupplierService } from '@app/services/supplier.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public navigation: NavItem[] = [];
  public selectedSupplierName: string;
  public isTradingUser: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private navigationService: NavigationService,
    private authService: AuthorizationService,
    private supplierService: SupplierService,
  ) { }

  ngOnInit() {
    this.supplierService.getSelectedSupplier().subscribe(supplier => {
      if (supplier) {
        this.selectedSupplierName = supplier.Name;
      } else {
        this.selectedSupplierName = '';
      }
    });
    this.navigationService.getMenu().subscribe(menu => {
      this.navigation = menu;
    });
    this.authService.getCurrentUser().subscribe(usr => {
      this.isTradingUser = (usr.Role === UserRole.TradingUser);
      if (usr.Role !== UserRole.Restricted) {
        this.navigationService.initMenu(usr);
      }
    });
  }
}
