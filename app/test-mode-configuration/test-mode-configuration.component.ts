import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserInfo } from '@app/models/user-info';
import { UserRole } from '@app/models/user-role';
import { IdName } from '@app/models/idName';
import { TestModeConfigurationService } from '@app/services/test-mode-configuration.service';
import { AuthorizationService } from '@app/services/authorization.service';
import { SupplierService } from '@app/services/supplier.service';
import { ValueLabel } from '@app/models/ui';

@Component({
  selector: 'test-mode-configuration',
  templateUrl: './test-mode-configuration.component.html',
  styleUrls: ['./test-mode-configuration.component.scss']
})
export class TestModeConfigurationComponent implements OnInit {

  private notSelectedOptionText = '--Not selected--';
  private unselectedSupplierOption: ValueLabel = { value: -1, label: this.notSelectedOptionText };
  public visible = false;
  private currentUser = new UserInfo();
  public roles: IdName[] = [
    { Name: 'Trading user', Id: UserRole.TradingUser },
    { Name: 'Supplier', Id: UserRole.Supplier },
    { Name: 'Translator', Id: UserRole.Translator },
    { Name: 'ExternalUser', Id: UserRole.ExternalUser },
    { Name: 'SupplierSign', Id: UserRole.SupplierSign },
    { Name: 'OneTimeLogin', Id: UserRole.OneTimeLogin },
  ];
  public suppliers: ValueLabel[] = [];
  public suppliersAreLoading: boolean;
  filterForm: FormGroup;

  constructor(
    private testModeConfigurationService: TestModeConfigurationService,
    private authorizationService: AuthorizationService,
    private supplierService: SupplierService,
    private fb: FormBuilder
  ) { }

  get isSetUserButtionEnabled() {
    return this.currentRole === UserRole.TradingUser
      || this.currentSupplierId > 0
      || (this.currentRole === UserRole.OneTimeLogin && this.currentSupplierId > 0);
  }
  get currentRole() {
    return +this.filterForm.get('Role').value;
  }

  get currentSupplierId() {
    return +this.filterForm.get('Supplier').value;
  }
  initForm() {
    this.filterForm = this.fb.group({
      Role: [],
      Supplier: []
    });
  }

  ngOnInit() {
    this.initForm();
    this.testModeConfigurationService.isTestModeEnabled()
      .subscribe(resp => {
        console.log('isTestModeEnabled = ' + resp);
        this.visible = resp;
      });

    this.authorizationService.getCurrentUser()
      .subscribe((x: UserInfo) => {
        this.currentUser = x;
        if ((this.currentUser.Role === UserRole.Supplier
          || this.currentUser.Role === UserRole.SupplierSign
          || this.currentUser.Role === UserRole.OneTimeLogin)
          && this.currentUser.SupplierId) {
          const supplierId = this.currentUser.SupplierId;
          this.suppliersAreLoading = true;
          this.supplierService.getSupplierById(supplierId)
            .subscribe(data => {
              this.suppliers = data ? [{ value: data.result.Id, label: data.result.Name }] : [];
              this.suppliersAreLoading = false;
              this.filterForm.patchValue({ Supplier: supplierId, Role: this.currentUser.Role });
              setTimeout(() => {
                // thin ice of shitcode. something wrong with select2 component. i'm sorry
                this.filterForm.patchValue({ Supplier: supplierId, Role: this.currentUser.Role });
              }, 1);
            });
        } else {
          this.suppliers = [this.unselectedSupplierOption];
          this.filterForm.patchValue({ Role: this.currentUser.Role, Supplier: <number>this.unselectedSupplierOption.value });
        }
      });
  }

  setEmulatedUser() {
    const user = new UserInfo();
    user.Role = this.currentRole;
    if (user.isTradingUser) {
      user.UserName = 'test user';
    } else {
      if (!this.currentSupplierId || this.currentSupplierId === this.unselectedSupplierOption.value) {
        return;
      }

      user.SupplierId = this.currentSupplierId;
      user.UserName = 'test supplier user';
    }

    this.testModeConfigurationService.setEmulatedUser(user);
    return false;
  }

  getCurrentUserRole(): string {
    if (!this.currentUser) {
      return '*no user*';
    }

    return this.getRoleName(this.currentUser.Role);
  }

  getRoleName(role: UserRole) {
    switch (role) {
      case UserRole.Restricted: return 'Restricted';
      case UserRole.TradingUser: return 'Trading user';
      case UserRole.Supplier: return 'Supplier';
      case UserRole.OneTimeLogin: return 'OneTimeLogin';
      case UserRole.ExternalUser: return 'External User';
      case UserRole.SupplierSign: return 'Supplier Sign';
    }

    return 'Unknown';
  }

  public onSearchSupplier(searchText: string) {
    this.suppliersAreLoading = true;
    this.supplierService.getSuppliersByName(searchText)
      .subscribe(suppliers => {
        this.suppliers = suppliers.result.map(x => <ValueLabel>{ value: x.Id, label: x.Name });
        this.suppliers.unshift(this.unselectedSupplierOption);
        this.suppliersAreLoading = false;
      },
        err => {
          console.log(err);
        },
        () => {
          this.suppliersAreLoading = false;
        }
      );
  }
}
