import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestModeConfigurationComponent } from './test-mode-configuration.component';
import { FormsModule } from '@angular/forms';
import { AutocompleteComponent } from '../shared/ui/field/autocomplete/autocomplete.component';
import { TestModeConfigurationService } from '../services/test-mode-configuration.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthorizationService } from '../services/authorization.service';
import { SupplierService } from '../services/supplier.service';

describe('TestModeConfigurationComponent', () => {
  let component: TestModeConfigurationComponent;
  let fixture: ComponentFixture<TestModeConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestModeConfigurationComponent ],
      imports: [FormsModule, AutocompleteComponent, HttpClientModule],
      providers: [TestModeConfigurationService, AuthorizationService, SupplierService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestModeConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
