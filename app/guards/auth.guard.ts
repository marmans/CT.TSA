import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthorizationService } from '@app/services/authorization.service';
import { UserRole } from '@app/models/user-role';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthorizationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.authService.getCurrentUserInfo().pipe(map(currentUser => {
      if (currentUser) {
        if (currentUser.Role === UserRole.SupplierSign
          || currentUser.Role === UserRole.Supplier
          || currentUser.Role === UserRole.OneTimeLogin) {
          if (state.url === '/supplier') {
            this.router.navigate(['supplier', currentUser.SupplierId]);
            return false;
          }
        }
        if (currentUser.Role === UserRole.OneTimeLogin) {
          const cpurl = `/supplier/${currentUser.SupplierId}/contactperson`;
          if (state.url === cpurl) {
            this.router.navigate(['supplier', currentUser.SupplierId, 'contactperson', currentUser.PersonId]);
            return false;
          }
        }
        if (currentUser.Role === UserRole.Restricted) {
          this.router.navigate(['/error']);
          return false;
        }
        // check if route is restricted by role
        if (route.data.roles && route.data.roles.indexOf(currentUser.Role) === -1) {
          // role not authorised so redirect to home page
          this.router.navigate(['/error']);
          return false;
        }
        // authorised so return true
        return true;
      }
      this.router.navigate(['/error']);
      return false;
    }));
  }
}
