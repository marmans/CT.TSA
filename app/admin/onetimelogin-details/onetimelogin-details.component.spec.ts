import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnetimeloginDetailsComponent } from './onetimelogin-details.component';

describe('OnetimeloginDetailsComponent', () => {
  let component: OnetimeloginDetailsComponent;
  let fixture: ComponentFixture<OnetimeloginDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnetimeloginDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnetimeloginDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
