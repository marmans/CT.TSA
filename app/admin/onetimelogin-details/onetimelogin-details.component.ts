import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ControlType } from '@app/models/ui';
import { OnetimeloginService } from '@app/services/onetimelogin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '@app/services/message.service';
import { SupplierService } from '@app/services/supplier.service';
import { numericValidator } from '@app/shared/custom-validators/numeric.validator';
import { CreateOneTimeLoginDto } from '@app/models/administration';

@Component({
  selector: 'app-onetimelogin-details',
  templateUrl: './onetimelogin-details.component.html',
  styleUrls: ['./onetimelogin-details.component.scss']
})
export class OnetimeloginDetailsComponent implements OnInit {
  loading: boolean;
  editMode = true;
  onetimeloginForm: FormGroup;
  controlTypeEnum = ControlType;

  constructor(private fb: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private onetimeloginService: OnetimeloginService,
    private messageService: MessageService,
    private supplierService: SupplierService) { }

  ngOnInit() {
    this.initForm();
    this.getEmptyPaymentTerms();
  }

  initForm() {
    this.onetimeloginForm = this.fb.group({
      Email: ['', [Validators.required, Validators.email]],
      CompanyName: ['', Validators.required],
      PaymentTerms: this.fb.array([])
    });
  }
  getEmptyPaymentTerms() {
    this.supplierService.getEmptyPaymentTermsList().subscribe(res => {
      const emptyPaymentTerms = res.result;
      for (let i = 0; i < emptyPaymentTerms.length; i++) {
        this.addPaymentTerm();
      }
      this.onetimeloginForm.patchValue({ PaymentTerms: emptyPaymentTerms });
    });
  }

  addPaymentTerm() {
    this.paymentTerms.push(this.getPaymentTermRow());
  }

  getPaymentTermRow() {
    return this.fb.group({
      NetDays: [null, numericValidator(true)],
      CashDiscount: [null, numericValidator(true)],
      Comment: [''],
      MarketId: [],
      MarketName: [''],
      SupplierCompanyId: []
    });
  }

  updatePaymentTermsBeforeSave(oneTimeLoginDto: CreateOneTimeLoginDto): CreateOneTimeLoginDto {
    oneTimeLoginDto.PaymentTerms.forEach(el => {
      if (el.NetDays) {
        el.NetDays = (el.NetDays + '').replace(',', '.');
      }
      if (el.CashDiscount) {
        el.CashDiscount = (el.CashDiscount + '').replace(',', '.');
      }
    });
    return oneTimeLoginDto;
  }

  save() {
    if (this.onetimeloginForm.valid) {
      this.loading = true;
      const formForSave = this.updatePaymentTermsBeforeSave(this.onetimeloginForm.value);
      this.onetimeloginService.createOneTimeLogin(formForSave)
        .subscribe(res => {
          this.loading = false;
          const validationResult = res.result.ValidationResult;
          if (validationResult.IsValid) {
            // this.router.navigate(['../../' + res.result.Id], { relativeTo: this.route });
            this.contactPersonCreated(res.result.Id);
          }
          validationResult.ValidationErrors.forEach(el => {
            console.log(el);
            const fc = this.onetimeloginForm.get(el.PropertyName);
            if (fc) {
              fc.setErrors({
                serverError: el.ErrorMessage
              });
            }
          });
        });
    }
  }

  cancel() {
    this.onetimeloginForm.reset();
  }

  contactPersonCreated(contactPersonId: number) {
    this.messageService.success('Account was created successfuly. Link and password was sent on email.');
  }

  get paymentTerms(): FormArray {
    return this.onetimeloginForm.get('PaymentTerms') as FormArray;
  }
}
