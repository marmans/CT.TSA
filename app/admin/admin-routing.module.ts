import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnetimeloginDetailsComponent } from './onetimelogin-details/onetimelogin-details.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'createonetimelogin',
        pathMatch: 'full'
      },
      {
        path: 'createonetimelogin',
        component: OnetimeloginDetailsComponent
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
