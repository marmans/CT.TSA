import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { OnetimeloginDetailsComponent } from './onetimelogin-details/onetimelogin-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SupplierModule } from '@app/supplier/supplier.module';

@NgModule({
  declarations: [OnetimeloginDetailsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    SupplierModule,
  ]
})
export class AdminModule { }
