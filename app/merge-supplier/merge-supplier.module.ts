import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MergeSupplierRoutingModule } from './merge-supplier-routing.module';
import { MergeHomeComponent } from './merge-home/merge-home.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MergeSupplierRoutingModule,
    SharedModule,
  ],
  declarations: [
    MergeHomeComponent,
  ]
})
export class MergeSupplierModule { }
