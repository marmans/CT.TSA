import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MergeHomeComponent } from './merge-home/merge-home.component';


const routes: Routes = [
  {
    path: '',
    component: MergeHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MergeSupplierRoutingModule { }
