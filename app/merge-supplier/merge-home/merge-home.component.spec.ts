import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeHomeComponent } from './merge-home.component';

describe('MergeHomeComponent', () => {
  let component: MergeHomeComponent;
  let fixture: ComponentFixture<MergeHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
