import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { UserInfo } from '@app/models/user-info';

@Injectable()
export class TestModeConfigurationService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  // ** Is user emulation mode enabled? */
  isTestModeEnabled(): Observable<boolean> {
    return this.http.get<boolean>(environment.sharepointServiceUrl + 'IsTestModeEnabled')
      .pipe(
        catchError(this.handleError<boolean>('IsTestModeEnabled'))
      );
  }

  setEmulatedUser(user: UserInfo) {
    this.http.post<UserInfo>(environment.sharepointServiceUrl + 'SetEmulatedUser',
      {
        user: user
      })
      .pipe(
        catchError(this.handleError<UserInfo>('SetEmulatedUser'))
      )
      .subscribe((result: UserInfo) => {
        if (result) {
          location.reload();
        }
      });
  }

  getVersionInfo() {
    return this.http.get<string>(environment.sharepointServiceUrl + 'GetVersion')
      .pipe(
        catchError(this.handleError<string>('GetVersion'))
      );
  }
}
