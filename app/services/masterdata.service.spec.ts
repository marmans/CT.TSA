import { TestBed } from '@angular/core/testing';

import { MasterDataService } from './masterdata.service';

describe('MasterDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasterDataService = TestBed.get(MasterDataService);
    expect(service).toBeTruthy();
  });
});
