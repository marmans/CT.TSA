import { TestBed } from '@angular/core/testing';

import { FilterForListsService } from './filter-for-lists.service';

describe('FilterForListsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilterForListsService = TestBed.get(FilterForListsService);
    expect(service).toBeTruthy();
  });
});
