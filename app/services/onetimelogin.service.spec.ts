import { TestBed } from '@angular/core/testing';

import { OnetimeloginService } from './onetimelogin.service';

describe('OnetimeloginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnetimeloginService = TestBed.get(OnetimeloginService);
    expect(service).toBeTruthy();
  });
});
