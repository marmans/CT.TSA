import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { Result, Pageable } from '@app/models/jsonrpc';
import { ContactPersonFilter, ContactPersonListItem, ContactPerson } from '@app/models/contact-person';
import { IdName } from '@app/models/idName';
import { SaveResult } from '@app/models/response';

@Injectable({
  providedIn: 'root'
})
export class ContactPersonService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  public getContactPersonList(filter: ContactPersonFilter): Observable<Result<ContactPersonListItem[]>> {
    return this.http.post<Result<ContactPersonListItem[]>>(environment.apiUrl, [{
      'method': 'GetContactPersonList',
      'params': { 'supplierId': filter.SupplierId, 'isActive': filter.ShowActive, 'hasLogin': filter.ShowPersonsWithLoginId },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<ContactPersonListItem[]>>('getContactPersonList'))
      );
  }

  getContactPersonById(id: number): Observable<Result<ContactPerson>> {
    return this.http.post<Result<ContactPerson>>(environment.apiUrl, [{
      'method': 'GetContactPersonById',
      'params': { 'personId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<ContactPerson>>('getContactPersonById'))
      );
  }

  saveContactPerson(person: ContactPerson) {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'SaveContactPerson',
      'params': { 'person': person },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SaveResult>>('saveContactPerson'))
      );
  }

  removeContactPerson(id: number) {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'RemoveContactPerson',
      'params': { 'personId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('removeContactPerson'))
      );
  }

  public getPersonTypes(): Observable<Result<IdName[]>> {
    return this.http.post<Result<IdName[]>>(environment.apiUrl, [{
      'method': 'GetPersonTypes',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<IdName[]>>('getPersonTypes'))
      );
  }
}
