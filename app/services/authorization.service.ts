import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer, of, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { UserInfo } from '@app/models/user-info';

@Injectable()
export class AuthorizationService extends CommonService {

  constructor(private http: HttpClient) {
    super();
    this.updateCurrentUser();
  }

  currentUser = new BehaviorSubject(new UserInfo());

  public getCurrentUser(): Observable<UserInfo> {
    return this.currentUser;
  }
  public get currentUserValue(): UserInfo {
    return this.currentUser.value;
  }

  public updateCurrentUser() {
    this.http.get<UserInfo>(environment.sharepointServiceUrl + 'GetCurrentUser')
      .pipe(
        catchError(this.handleError<UserInfo>('GetCurrentUser'))
      )
      .subscribe(result => {
        this.currentUser.next(result);
      }, error => {
        this.currentUser.error(error);
      });
  }

  public getCurrentUserInfo() {
    return this.http.get<UserInfo>(environment.sharepointServiceUrl + 'GetCurrentUser')
      .pipe(
        catchError(this.handleError<UserInfo>('GetCurrentUser')),
        map(res => {
          const tmpUsr = this.currentUser.value;
          if (tmpUsr.Role !== res.Role ||
            tmpUsr.SupplierId !== res.SupplierId ||
            tmpUsr.PersonId !== res.PersonId ||
            tmpUsr.UserName !== res.UserName
            ) {
              this.currentUser.next(res);
          }
          return res;
        })
      );
  }
}
