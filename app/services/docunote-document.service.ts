import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { Result } from '@app/models/jsonrpc';
import { DocunoteDocument, DocumentStatus } from '@app/models/document';

@Injectable({
  providedIn: 'root'
})
export class DocunoteDocumentService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  getDocumentsBySupplierId(id: number): Observable<Result<DocunoteDocument[]>> {
    return this.http.post<Result<DocunoteDocument[]>>(environment.apiUrl, [{
      'method': 'GetDocumentsBySupplierId',
      'params': { 'supplierId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DocunoteDocument[]>>('getDocumentsBySupplierId'))
      );
  }

  signDocuments(supplierId: number, documents: DocunoteDocument[]): Observable<Result<boolean>> {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'SignDocuments',
      'params': { 'supplierId': supplierId, 'documents': documents },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('signDocuments'))
      );
  }
}
