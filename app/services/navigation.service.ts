import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NavItem } from '../models/navigation';
import { Router, NavigationEnd } from '@angular/router';
import { AuthorizationService } from './authorization.service';
import { UserRole } from '@app/models/user-role';
import { UserInfo } from '@app/models/user-info';
import { SupplierService } from './supplier.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  navigationItems: BehaviorSubject<NavItem[]> = new BehaviorSubject([]);
  menu: NavItem[] = [];

  constructor(private router: Router,
    private authService: AuthorizationService,
    private supplierService: SupplierService
  ) { }
  // Obsolete
  private initDefaultMenuObject(): NavItem[] {
    const menu: NavItem[] = [
      {
        Name: 'Suppliers',
        Path: 'supplier',
        Roles: [UserRole.TradingUser, UserRole.Supplier],
        Children: [{
          Name: 'Supplier Information',
          Path: 'supplier/${supplierid}/${suppliermode}'
        },
        {
          Name: 'Supplier sites',
          Path: 'supplier/${supplierid}/site',
          Children: [{
            Name: 'Details',
            Path: 'supplier/${supplierid}/site/${siteid}/${sitemode}'
          }]
        },
        {
          Name: 'Contact persons',
          Path: 'supplier/${supplierid}/contactperson',
          Children: [{
            Name: 'Details',
            Path: 'supplier/${supplierid}/contactperson/${cpid}/${cpmode}'
          }]
        },
        {
          Name: 'Defects',
          Path: 'supplier/${supplierid}/defect',
          Children: [{
            Name: 'Details',
            Path: 'supplier/${supplierid}/defect/${defectid}/${dmode}'
          }]
        }]
      },
      {
        Name: 'Pending Sites',
        Path: 'pendingsite',
        Roles: [UserRole.TradingUser, UserRole.Supplier],
      },
      {
        Name: 'Merge Suppliers',
        Path: 'mergesupplier',
        Roles: [UserRole.TradingUser],
      },
      {
        Name: 'High Risk Countries',
        Path: 'highriskcountries',
        Roles: [UserRole.TradingUser],
      },
      {
        Name: '',
        Path: '',
        IsSeparator: true,
      },
      {
        Name: 'Administration',
        Path: 'admin',
        Roles: [UserRole.TradingUser],
        Children: [{
          Name: 'One time login',
          Path: 'admin/createonetimelogin'
        }]
      }
    ];
    return menu;
  }

  // Obsolete
  initMenuObject(userInfo: UserInfo) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const menu = this.initDefaultMenuObject();
        const url = event.urlAfterRedirects.split('/');
        url.shift();
        let newMenu = this.applyRoles(menu, userInfo.Role);
        newMenu = this.applyVariables(newMenu, url);
        newMenu = this.applyVisibility(newMenu, url);
        this.navigationItems.next(this.copy(newMenu));
      }
    });
  }

  copy(value) {
    return JSON.parse(JSON.stringify(value));
  }

  recursiveApplyChildrenRole(items: NavItem[], userRole: UserRole): NavItem[] {
    const newMenu: NavItem[] = [];
    items.forEach(el => {
      const roles = el.Roles;
      // check if route is restricted by role
      if (roles && roles.indexOf(userRole) === -1) {
        // role not authorised so dont add it
        return;
      }
      if (el.Children) {
        el.Children = this.recursiveApplyChildrenRole(el.Children, userRole);
      }

      newMenu.push(el);
    });
    return newMenu;
  }
  applyRoles(menu: NavItem[], userRole: UserRole) {
    let newMenu: NavItem[] = [];
    newMenu = this.recursiveApplyChildrenRole(menu, userRole);
    return newMenu;
  }

  applyVariables(menu: NavItem[], url: string[]) {
    let supplierid = '0';
    let suppliermode = 'edit';
    let siteid = '0';
    let cpid = '0';
    let defectid = '0';
    let sitemode = 'edit';
    let cpmode = 'edit';
    let dmode = 'edit';

    if (url.length > 1) {
      if (url[0] === 'supplier') {
        supplierid = url[1];
        suppliermode = (url[2] === 'edit') ? 'edit' : 'view';


        if (url[2] === 'site') {
          if (url.length > 3) {
            siteid = url[3];
            sitemode = (url[4] === 'edit') ? 'edit' : 'view';
          }
        }

        if (url[2] === 'contactperson') {
          if (url.length > 3) {
            cpid = url[3];
            cpmode = (url[4] === 'edit') ? 'edit' : 'view';
          }
        }

        if (url[2] === 'defect') {
          if (url.length > 3) {
            defectid = url[3];
            dmode = (url[4] === 'edit') ? 'edit' : 'view';
          }
        }
      }
    }
    const replaceVariables = function (path: string) {
      return path
        .replace('${supplierid}', supplierid)
        .replace('${suppliermode}', suppliermode)
        .replace('${siteid}', siteid)
        .replace('${cpid}', cpid)
        .replace('${defectid}', defectid)
        .replace('${sitemode}', sitemode)
        .replace('${dmode}', dmode)
        .replace('${cpmode}', cpmode);
    };

    menu.forEach(el => {
      el.Path = replaceVariables(el.Path);
      if (el.Children) {
        el.Children.forEach(el2 => {
          el2.Path = replaceVariables(el2.Path);
          if (el2.Children) {
            el2.Children.forEach(el3 => {
              el3.Path = replaceVariables(el3.Path);
            });
          }
        });
      }
    });
    return menu;
  }


  applyVisibility(menu: NavItem[], url: string[]) {
    const newMenu: NavItem[] = [];
    menu.forEach(el => {
      const newEl: NavItem = JSON.parse(JSON.stringify(el));
      // usually - lists
      if (url.length === 1) {
        newEl.Children = [];
      }
      // usually - first level details page
      if (url.length === 3) {
        if (newEl.Children) {
          newEl.Children.forEach(el2 => {
            el2.Children = [];
          });
        }
      }
      // usually - second level details page
      // check if url is same as path. show it if true
      if (url.length === 5) {
        if (newEl.Children) {
          newEl.Children.forEach(el2 => {
            const children = el2.Children;
            const newChildren: NavItem[] = [];
            if (children) {
              el2.Children.forEach(el3 => {
                if (el3.Path === url.join('/')) {
                  newChildren.push(el3);
                }
              });
              el2.Children = newChildren;
            }
          });
        }
      }
      newMenu.push(newEl);
    });
    return newMenu;
  }

  initMenu(userInfo: UserInfo) {
    const supplier = 'supplier';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const supplierList: NavItem = {
          Name: 'Suppliers',
          Path: supplier,
          Roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.SupplierSign]
        };
        const pendingSite: NavItem = {
          Name: 'Pending Sites',
          Path: 'pendingsite',
          Roles: [UserRole.TradingUser],
        };
        const mergeSupplier: NavItem = {
          Name: 'Merge Suppliers',
          Path: 'mergesupplier',
          Roles: [UserRole.TradingUser],
        };
        const highRiskCountries: NavItem = {
          Name: 'High Risk Countries',
          Path: 'highriskcountries',
          Roles: [UserRole.TradingUser],
        };
        const separator: NavItem = {
          Name: '',
          Path: '',
          IsSeparator: true,
          Roles: [UserRole.TradingUser],
        };
        let supplierIdForOneTimeLogin = 0;
        let contactPersonIdForOneTimeLogin = 0;
        if (userInfo.Role === UserRole.OneTimeLogin) {
          supplierIdForOneTimeLogin = userInfo.SupplierId;
          contactPersonIdForOneTimeLogin = userInfo.PersonId;
        }
        let supplierInformationForOneTimeLoginUser: NavItem = {
          Name: 'Supplier Information',
          Path: `${supplier}/${supplierIdForOneTimeLogin}`,
          IsSeparator: false,
          Roles: [UserRole.OneTimeLogin],
        };
        let contactPersonInformationForOneTimeLoginUser: NavItem = {
          Name: 'Contact person information',
          Path: `${supplier}/${supplierIdForOneTimeLogin}/contactperson/${contactPersonIdForOneTimeLogin}`,
          IsSeparator: false,
          Roles: [UserRole.OneTimeLogin],
        };

        const url = event.urlAfterRedirects.split('/');
        url.shift();
        // for trading users, suppliers
        if (url.length > 1) {
          if (url[0] === supplier) {
            const id = url[1];
            const mode = (url[2] === 'edit') ? 'edit' : 'view';

            const supplierDetails: NavItem = {
              Name: 'Supplier Information',
              Path: `${supplier}/${id}/${mode}`,
              Roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.SupplierSign]
            };
            supplierInformationForOneTimeLoginUser = {
              Name: 'Supplier Information',
              Path: `${supplier}/${supplierIdForOneTimeLogin}/${mode}`,
              Roles: [UserRole.OneTimeLogin],
            };
            const siteList: NavItem = {
              Name: 'Supplier sites',
              Path: `${supplier}/${id}/site`,
              Roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.SupplierSign]
            };
            const docList: NavItem = {
              Name: 'Documents',
              Path: `${supplier}/${id}/document`
            };
            if (url[2] === 'site') {
              if (url.length > 3) {
                siteList.Children = [this.generateDetailsChild(url)];
              }
            }

            const contactPersonList: NavItem = {
              Name: 'Contact persons',
              Path: `${supplier}/${id}/contactperson`,
              Roles: [UserRole.TradingUser, UserRole.Supplier, UserRole.SupplierSign]
            };
            if (url[2] === 'contactperson') {
              if (url.length > 3) {
                contactPersonList.Children = [this.generateDetailsChild(url)];
                const ctmode = (url[4] === 'edit') ? 'edit' : 'view';
                contactPersonInformationForOneTimeLoginUser = {
                  Name: 'Contact person information',
                  Path: `${supplier}/${supplierIdForOneTimeLogin}/contactperson/${contactPersonIdForOneTimeLogin}/${ctmode}`,
                  IsSeparator: false,
                  Roles: [UserRole.OneTimeLogin],
                };
              }
            }

            const defectList: NavItem = {
              Name: 'Defects',
              Path: `${supplier}/${id}/defect`,
              Roles: [UserRole.TradingUser]
            };
            if (url[2] === 'defect') {
              if (url.length > 3) {
                defectList.Children = [this.generateDetailsChild(url)];
              }
            }

            supplierList.Children = [supplierDetails];
            if (id !== '0') {
              supplierList.Children.push(siteList);
              supplierList.Children.push(contactPersonList);
              supplierList.Children.push(defectList);
              supplierList.Children.push(docList);

              if (userInfo.Role === UserRole.TradingUser) {
                const selectedSupplier = this.supplierService.getSelectedSupplier();
                if (selectedSupplier.value === null || (selectedSupplier.value && selectedSupplier.value.Id !== +id)) {
                  if (url[2] === 'site' || url[2] === 'defect' || url[2] === 'contactperson') {
                    this.supplierService.getSupplierWithAddressesById(+id).subscribe(res => {
                      // reload selected supplier
                    });
                  }
                }
              }
            }
          }
        }
        if (url.length >= 1) {
          if (url[0] !== supplier || (url.length === 1 && url[0] === supplier)) {
            // reset supplier
            this.supplierService.resetSelectedSupplier();
          }
        }

        const admin = this.getAdminMenu(url);
        let menu = [supplierList,
          pendingSite,
          mergeSupplier,
          highRiskCountries,
          separator,
          admin,
          supplierInformationForOneTimeLoginUser,
          contactPersonInformationForOneTimeLoginUser
        ];
        menu = this.applyRoles(menu, userInfo.Role);
        this.navigationItems.next(menu);
      }
    });
  }

  getMenu(): BehaviorSubject<NavItem[]> {
    return this.navigationItems;
  }

  getAdminMenu(url: string[]): NavItem {
    return {
      Name: 'Create new supplier',
      Path: 'admin/createonetimelogin',
      Roles: [UserRole.TradingUser]
    };
  }

  private generateDetailsChild(url: string[]) {
    return {
      Name: 'Details',
      Path: `${url.join('/')}`
    };
  }
}
