import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { Result, Pageable } from '@app/models/jsonrpc';
import { Language } from '@app/models/language';
import { Country, CountryPort } from '@app/models/country';

@Injectable({
  providedIn: 'root'
})
export class MasterDataService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  public getLanguages(): Observable<Result<Language[]>> {
    return this.http.post<Result<Language[]>>(environment.apiUrl, [{
      'method': 'GetLanguages',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Language[]>>('getLanguages'))
      );
  }

  public getCountries(): Observable<Result<Country[]>> {
    return this.http.post<Result<Country[]>>(environment.apiUrl, [{
      'method': 'GetCountries',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Country[]>>('getCountries'))
      );
  }

  public getCountryPortsBySearchText(searchText: string): Observable<Result<CountryPort[]>> {
    return this.http.post<Result<CountryPort[]>>(environment.apiUrl, [{
      'method': 'GetCountryPortsBySearchText',
      'params': { 'searchText': searchText },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<CountryPort[]>>('getCountryPortsBySearchText'))
      );
  }

  public getCountryPort(id: number): Observable<Result<CountryPort>> {
    return this.http.post<Result<CountryPort>>(environment.apiUrl, [{
      'method': 'GetCountryPort',
      'params': { 'id': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<CountryPort>>('getCountryPort'))
      );
  }
}
