import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ParamMap } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FilterForListsService {
  private static readonly MaximumPageSize = 150;
  private defaultParams;
  private filterParams = {};

  constructor() { }
  public init<T>(defaultParams: T) {
    this.defaultParams = defaultParams;
  }

  public getFilterParamsFromRouteParamMap<T>(params: ParamMap): Observable<T> {
    this.filterParams = <T>{};
    const props = Object.getOwnPropertyNames(this.defaultParams);
    props.forEach(el => {
      const value = this.defaultParams[el];
      if (typeof value === 'number') {
        this.filterParams[el] = params.get(el) ? +params.get(el) : this.defaultParams[el];
      } else if (typeof value === 'string') {
        this.filterParams[el] = params.get(el) ? params.get(el) : this.defaultParams[el];
      } else if (value instanceof Array) {
        this.filterParams[el] = params.get(el) ? this.parseArray(params.get(el)) : [];
        // to send default params user this.
        // By default it will be empty array
        // this.defaultParams[el];
      } else if (typeof value === 'boolean') {
        this.filterParams[el] = params.get(el) ? (params.get(el) === 'true' ) : this.defaultParams[el];
      }
    });

    if (this.filterParams['PageSize'] > FilterForListsService.MaximumPageSize) {
      this.filterParams['PageSize'] = FilterForListsService.MaximumPageSize;
    }
    return of(<T>this.filterParams);
  }
  public parseArray(value: string) {
    const arr = value.split(',');
    return arr.map(el => {
      return parseInt(el, 10);
    });
  }

  public getRouteParamsFromFilterParams<T>(filterParams: T): Observable<any> {
    const urlParams = Object.assign({}, filterParams);
    for (const urlParam in urlParams) {
      if (urlParams[urlParam] === this.defaultParams[urlParam]) {
        delete urlParams[urlParam];
      }
    }
    return of(urlParams);
  }
}
