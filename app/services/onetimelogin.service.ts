import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';
import { CreateOneTimeLoginDto } from '@app/models/administration';
import { Observable } from 'rxjs';
import { Result } from '@app/models/jsonrpc';
import { environment } from '@env/environment';
import { catchError } from 'rxjs/operators';
import { SaveResult } from '@app/models/response';
import { OneTimeLoginStatusDto } from '@app/models/user-info';

@Injectable({
  providedIn: 'root'
})
export class OnetimeloginService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  createOneTimeLogin(onetimeLogin: CreateOneTimeLoginDto): Observable<Result<SaveResult>> {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'CreateOneTimeLogin',
      'params': { 'createOneTimeLoginDto': onetimeLogin },
      'id': 1
    }])
    .pipe(
      catchError(this.handleError<Result<SaveResult>>('CreateOneTimeLogin'))
    );
  }

  getOneTimeLoginStatus(personId: number): Observable<Result<OneTimeLoginStatusDto>> {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'GetOneTimeLoginStatus',
      'params': { 'personId': personId },
      'id': 1
    }])
    .pipe(
      catchError(this.handleError<Result<OneTimeLoginStatusDto>>('getOneTimeLoginStatus'))
    );
  }
}
