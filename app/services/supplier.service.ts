import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer, of, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CommonService } from './common.service';
import { Result, Pageable } from '@app/models/jsonrpc';
import { SupplierListItem, Supplier, SupplierListFilter, PaymentTerm } from '@app/models/supplier';
import { ValidationResult, SaveResult } from '@app/models/response';

@Injectable()
export class SupplierService extends CommonService {
  private selectedSupplier: BehaviorSubject<Supplier> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
    super();
  }

  getSuppliers(filter: SupplierListFilter): Observable<Result<Pageable<SupplierListItem[]>>> {
    return this.http.post<Result<Pageable<SupplierListItem[]>>>(environment.apiUrl, [{
      'method': 'GetSupplierList',
      'params': { 'filter': filter },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Pageable<SupplierListItem[]>>>('getSuppliers'))
      );
  }

  getSuppliersByName(searchText: string): Observable<Result<SupplierListItem[]>> {
    return this.http.post<Result<SupplierListItem[]>>(environment.apiUrl, [{
      'method': 'GetSuppliersByName',
      'params': { 'searchText': searchText },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SupplierListItem[]>>('getSuppliers'))
      );
  }

  getSupplierById(id: number): Observable<Result<SupplierListItem>> {
    return this.http.post<Result<SupplierListItem>>(environment.apiUrl, [{
      'method': 'GetSupplierById',
      'params': { 'id': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SupplierListItem>>('getSupplierById'))
      );
  }

  getSelectedSupplier() {
    return this.selectedSupplier;
  }
  resetSelectedSupplier() {
    this.selectedSupplier.next(null);
  }

  getSupplierWithAddressesById(id: number): Observable<Result<Supplier>> {
    return this.http.post<Result<Supplier>>(environment.apiUrl, [{
      'method': 'GetSupplierWithAddressesById',
      'params': { 'supplierId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Supplier>>('getSupplierWithAddressesById')),
        map(res => {
          const supplier = res.result;
          const selSupplier = this.selectedSupplier.value;
          if (!selSupplier || supplier.Id !== selSupplier.Id) {
            this.selectedSupplier.next(supplier);
          }
          return res;
        })
      );
  }

  saveSupplier(supplier: Supplier) {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'SaveSupplierWithAddresses',
      'params': { 'supplier': supplier },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SaveResult>>('saveSupplier'))
      );
  }

  blockSupplier(supplier: Supplier) {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'BlockSupplier',
      'params': { 'supplier': supplier },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('blockSupplier'))
      );
  }

  unblockSupplier(supplierId: number) {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'UnblockSupplier',
      'params': { 'supplierId': supplierId },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('unblockSupplier'))
      );
  }

  getEmptyPaymentTermsList(): Observable<Result<PaymentTerm[]>> {
    return this.http.post<Result<PaymentTerm[]>>(environment.apiUrl, [{
      'method': 'GetEmptyPaymentTermsList',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<PaymentTerm[]>>('getEmptyPaymentTermsList'))
      );
  }
}
