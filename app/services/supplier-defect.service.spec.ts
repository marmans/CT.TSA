import { TestBed } from '@angular/core/testing';

import { SupplierDefectService } from './supplier-defect.service';

describe('SupplierDefectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SupplierDefectService = TestBed.get(SupplierDefectService);
    expect(service).toBeTruthy();
  });
});
