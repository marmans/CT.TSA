import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Result, Pageable } from '@app/models/jsonrpc';
import { environment } from '@env/environment';
import {
  SupplierDefectListItem, SupplierDefectProduct, SupplierDefectFilter, DefectReportedSource,
  DefectClosingRemark, DefectFollowUp, DefectCategory, DefectReceivedSource
} from '@app/models/supplier-defect';
import { catchError } from 'rxjs/operators';
import { SaveResult } from '@app/models/response';

@Injectable({
  providedIn: 'root'
})
export class SupplierDefectService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  public getSupplierDefectsList(filter: SupplierDefectFilter): Observable<Result<Pageable<SupplierDefectListItem[]>>> {
    return this.http.post<Result<Pageable<SupplierDefectListItem[]>>>(environment.apiUrl, [{
      'method': 'GetSupplierDefectsBySupplierId',
      'params': { 'filter': filter },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Pageable<SupplierDefectListItem[]>>>('getSupplierDefectsBySupplierId'))
      );
  }

  public getDefectReportedSources() {
    return this.http.post<Result<DefectReportedSource[]>>(environment.apiUrl, [{
      'method': 'GetDefectReportedSources',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DefectReportedSource[]>>('getDefectReportedSources'))
      );
  }

  public getDefectReceivedSources() {
    return this.http.post<Result<DefectReceivedSource[]>>(environment.apiUrl, [{
      'method': 'GetDefectReceivedSources',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DefectReceivedSource[]>>('getDefectReceivedSources'))
      );
  }

  public getDefectCategories() {
    return this.http.post<Result<DefectCategory[]>>(environment.apiUrl, [{
      'method': 'GetDefectCategories',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DefectCategory[]>>('getDefectCategories'))
      );
  }

  public getDefectFollowUps() {
    return this.http.post<Result<DefectFollowUp[]>>(environment.apiUrl, [{
      'method': 'GetDefectFollowUps',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DefectFollowUp[]>>('getDefectFollowUps'))
      );
  }

  public getDefectClosingRemarks() {
    return this.http.post<Result<DefectClosingRemark[]>>(environment.apiUrl, [{
      'method': 'GetDefectClosingRemarks',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<DefectClosingRemark[]>>('getDefectClosingRemarks'))
      );
  }

  public getDataForSupplierDefect(supplierDefectId: number) {
    const req = [
      { 'method': 'GetDefectReportedSources', 'params': {}, 'id': 1 },
      { 'method': 'GetDefectReceivedSources', 'params': {}, 'id': 2 },
      { 'method': 'GetDefectCategories', 'params': {}, 'id': 3 },
      { 'method': 'GetDefectFollowUps', 'params': {}, 'id': 4 },
      { 'method': 'GetDefectClosingRemarks', 'params': {}, 'id': 5 }];
    if (supplierDefectId > 0) {
      req.push({ 'method': 'GetSupplierDefectById', 'params': { 'supplierDefectId': supplierDefectId }, 'id': 6 });
    }
    return this.http.post<any[]>(environment.apiUrl, req)
      .pipe(
        catchError(this.handleError<any[]>('getDataForSupplierDefect'))
      );
  }

  public getSupplierGtinsBySearchText(supplierId: number, searchText: string) {
    return this.http.post<Result<number[]>>(environment.apiUrl, [{
      'method': 'GetSupplierGtinsBySearchText',
      'params': { 'supplierId': supplierId, 'searchText': searchText },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<number[]>>('getSupplierGtinsBySearchText'))
      );
  }

  public getSupplierDefectProduct(supplierId: number, gtin: number) {
    return this.http.post<Result<SupplierDefectProduct>>(environment.apiUrl, [{
      'method': 'GetSupplierDefectProduct',
      'params': { 'supplierId': supplierId, 'gtin': gtin },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SupplierDefectProduct>>('getSupplierDefectProduct'))
      );
  }

  public saveSupplierDefect(supplierDefect) {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'SaveSupplierDefect',
      'params': { 'supplierDefectDto': supplierDefect },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SaveResult>>('updateSupplier'))
      );
  }
}
