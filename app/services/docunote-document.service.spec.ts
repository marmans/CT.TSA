import { TestBed } from '@angular/core/testing';

import { DocunoteDocumentService } from './docunote-document.service';

describe('DocunoteDocumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocunoteDocumentService = TestBed.get(DocunoteDocumentService);
    expect(service).toBeTruthy();
  });
});
