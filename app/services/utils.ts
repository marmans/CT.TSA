import { ValidatorFn, AbstractControl } from '@angular/forms';
import { IMyDate } from 'mydatepicker';
import { PaymentTerm } from '@app/models/supplier';

export class Utils {

    public static ToIMyDate(jsDateString: string): IMyDate {
        const date = new Date(jsDateString);
        const ss: IMyDate = { day: date.getDay(), year: date.getFullYear(), month: date.getMonth() };
        return ss;
    }

    public static FromServerStringDateToString(val): string {
        if (val) {
            const b = new Date(val);
            const date = b.getDate();
            const month = b.getMonth() + 1;
            const year = b.getFullYear();
            return this.LeadingZero(date) + '.' + this.LeadingZero(month) + '.' + year;
        }
        return '';
    }

    public static LeadingZero(val: number): string {
        let result = '';
        const tmp = '0' + val.toString();
        result = tmp.slice(-2);
        return result;
    }

    public static toIMyDate(jsDateString: string): IMyDate {
        const res = new Date(jsDateString.replace('Z', ''));
        return { month: res.getMonth() + 1, year: res.getFullYear(), day: res.getDate() };
    }

    public static toJsDateString(date: IMyDate): string {
        return new Date(Date.UTC(date.year, date.month - 1, date.day)).toISOString().replace('Z', '');
    }

    public static getTooltipTextForSiteDelete(): string {
        return 'The site is used in one or more specifications and can not be deleted';
    }

    public static getMessageForOneTimeLoginStatus(isPersonOneTime: boolean, isSupplierOneTime: boolean) {
        const fillWhat: string[] = [];
        if (isPersonOneTime) {
            fillWhat.push('<li>Contact Person Information</li>');
        }
        if (isSupplierOneTime) {
            fillWhat.push('<li>Supplier Information</li>');
        }
        return ('Please, fill: <ul>' + fillWhat.join('') + '</ul>');
    }
}
