import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { SaveResult } from '@app/models/response';
import { Result, Pageable } from '@app/models/jsonrpc';
import { SiteListFilter, Address, PendingSiteFilter } from '@app/models/site';
import { IdName } from '@app/models/idName';

@Injectable({
  providedIn: 'root'
})
export class SiteService extends CommonService {

  constructor(private http: HttpClient) {
    super();
  }

  getSupplierSiteList(filter: SiteListFilter): Observable<Result<Pageable<Address[]>>> {
    return this.http.post<Result<Pageable<Address[]>>>(environment.apiUrl, [{
      'method': 'GetSupplierSiteList',
      'params': { 'filter': filter },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Pageable<Address[]>>>('GetSupplierSiteList'))
      );
  }

  getPendingSiteList(filter: PendingSiteFilter): Observable<Result<Pageable<Address[]>>> {
    return this.http.post<Result<Pageable<Address[]>>>(environment.apiUrl, [{
      'method': 'GetPendingSites',
      'params': { 'filter': filter },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Pageable<Address[]>>>('GetPendingSitesList'))
      );
  }

  getSupplierSite(id: number): Observable<Result<Address>> {
    return this.http.post<Result<Address>>(environment.apiUrl, [{
      'method': 'GetSupplierSite',
      'params': { 'addressId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<Address>>('getSupplierSite'))
      );
  }

  saveSupplierSite(address: Address) {
    return this.http.post<Result<SaveResult>>(environment.apiUrl, [{
      'method': 'SaveSupplierSite',
      'params': { 'address': address },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<SaveResult>>('saveSupplierSite'))
      );
  }

  removeSupplierSite(id: number) {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'RemoveSupplierSite',
      'params': { 'addressId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('removeSupplierSite'))
      );
  }

  disableSupplierSite(id: number) {
    return this.http.post<Result<boolean>>(environment.apiUrl, [{
      'method': 'DisableSupplierSite',
      'params': { 'addressId': id },
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<boolean>>('removeSupplierSite'))
      );
  }

  getAddressStatuses(): Observable<Result<IdName[]>> {
    return this.http.post<Result<IdName[]>>(environment.apiUrl, [{
      'method': 'GetAddressStatuses',
      'params': {},
      'id': 1
    }])
      .pipe(
        catchError(this.handleError<Result<IdName[]>>('getAddressStatuses'))
      );
  }

  getAddressTypes(): Observable<Result<IdName[]>> {
    const r: Result<IdName[]> = {
      result: this.getAddressTypesForSite(),
      id: 1,
      jsonrpc: '2.0',
      error: null
    };

    return of(r);
  }

  getAddressTypesForSite() {
    return [
      new IdName(5, 'Packing'),
      new IdName(6, 'Production'),
      new IdName(9, 'Warehouse'),
      new IdName(10, 'Pickup')
    ];
  }
}
