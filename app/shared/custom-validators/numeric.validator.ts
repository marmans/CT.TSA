import { AbstractControl, ValidatorFn } from '@angular/forms';

export function numericValidator(nullable = false): ValidatorFn {
    const regexp: RegExp = /^[-+]?[0-9]*\,?[0-9]+([eE][-+]?[0-9]+)?$/i;
    return (control: AbstractControl): {[key: string]: any} | null => {
        if (nullable && (control.value === '' || control.value === null)) {
            return null;
        }
        const matches = regexp.test(control.value);
        return matches ? null : { 'numeric': {value: control.value}};
    };
}
