import { AbstractControl, ValidatorFn } from '@angular/forms';

export function vatValidator(nullable = false): ValidatorFn {
  const regexp: RegExp = /^[a-zA-Z]{2}.*$/i;
  return (control: AbstractControl): {[key: string]: any} | null => {
      if (nullable && (control.value === '' || control.value === null)) {
          return null;
      }
      const matches = regexp.test(control.value);
      return matches ? null : { 'vat': {value: control.value}};
  };
}
