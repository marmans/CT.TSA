import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextboxComponent } from './ui/field/textbox/textbox.component';
import { CheckboxComponent } from './ui/field/checkbox/checkbox.component';
import { DropdownComponent } from './ui/field/dropdown/dropdown.component';
import { CrudButtonBarComponent } from './ui/crud-button-bar/crud-button-bar.component';
import { ModalDialogComponent } from './ui/modal-dialog/modal-dialog.component';
import { TwoColumnRowComponent } from './ui/table/two-column-row/two-column-row.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YesOrNoRadioComponent } from './ui/field/yes-or-no-radio/yes-or-no-radio.component';
import { SectionHeaderComponent } from './ui/section-header/section-header.component';
import { ButtonComponent } from './ui/button/button.component';
import { RequiredSignComponent } from './ui/field/required-sign/required-sign.component';
import { ValidationComponent } from './ui/field/validation/validation.component';
import { MaincontactsComponent } from './ui/field/maincontacts/maincontacts.component';
import { CheckboxMultiComponent } from './ui/field/checkbox-multi/checkbox-multi.component';
import { DatepickerComponent } from './ui/field/datepicker/datepicker.component';
import { MyDatePickerModule } from 'mydatepicker';
import { TextareaComponent } from './ui/field/textarea/textarea.component';
import { LoaderBlockerComponent } from './ui/loader-blocker/loader-blocker.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { TooltipComponent } from './ui/tooltip/tooltip.component';
import { CellComponent } from './ui/table/cell/cell.component';
import { TypeaheadComponent } from './ui/field/typeahead/typeahead.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { RemoveRowBtnComponent } from './ui/table/remove-row-btn/remove-row-btn.component';
import { MomentModule } from 'ngx-moment';
import { AttentionMarkComponent } from './ui/mark/attention-mark/attention-mark.component';

@NgModule({
  declarations: [
    TextboxComponent,
    CheckboxComponent,
    DropdownComponent,
    CrudButtonBarComponent,
    ModalDialogComponent,
    TwoColumnRowComponent,
    YesOrNoRadioComponent,
    SectionHeaderComponent,
    ButtonComponent,
    RequiredSignComponent,
    ValidationComponent,
    MaincontactsComponent,
    CheckboxMultiComponent,
    DatepickerComponent,
    TextareaComponent,
    LoaderBlockerComponent,
    TooltipComponent,
    CellComponent,
    TypeaheadComponent,
    RemoveRowBtnComponent,
    AttentionMarkComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    TooltipModule,
    NgSelectModule,
    MomentModule
  ],

  exports: [
    TextboxComponent,
    CheckboxComponent,
    DropdownComponent,
    CrudButtonBarComponent,
    ModalDialogComponent,
    TwoColumnRowComponent,
    YesOrNoRadioComponent,
    SectionHeaderComponent,
    ButtonComponent,
    RequiredSignComponent,
    ValidationComponent,
    MaincontactsComponent,
    CheckboxMultiComponent,
    DatepickerComponent,
    LoaderBlockerComponent,
    TooltipComponent,
    CellComponent,
    TypeaheadComponent,
    RemoveRowBtnComponent,
    AttentionMarkComponent,
  ]
})
export class SharedModule { }
