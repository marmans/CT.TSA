import { Component, OnInit, Input } from '@angular/core';
import { TestModeConfigurationService } from '@app/services/test-mode-configuration.service';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent implements OnInit {
  @Input() headerText: string;
  @Input() showVersion: boolean;
  version: string;

  constructor(
    private testModeConfigurationService: TestModeConfigurationService
  ) { }

  ngOnInit() {
    this.getVersion();
  }

  getVersion() {
    this.testModeConfigurationService.getVersionInfo()
      .subscribe(res => {
        this.version = res;
      });
  }
}
