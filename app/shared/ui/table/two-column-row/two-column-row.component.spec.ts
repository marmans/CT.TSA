import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoColumnRowComponent } from './two-column-row.component';

describe('TwoColumnRowComponent', () => {
  let component: TwoColumnRowComponent;
  let fixture: ComponentFixture<TwoColumnRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoColumnRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoColumnRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
