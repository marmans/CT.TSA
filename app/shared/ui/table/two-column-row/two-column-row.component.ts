import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, AbstractControl, FormArray } from '@angular/forms';
import { ControlType, TextboxType } from '@app/models/ui';
import { IdName } from '@app/models/idName';

@Component({
  selector: 'app-two-column-row',
  templateUrl: './two-column-row.component.html',
  styleUrls: ['./two-column-row.component.scss']
})
export class TwoColumnRowComponent implements OnInit {
  @Input() headerText: string;
  @Input() required: boolean;

  @Input() editMode: boolean;

  @Input() group: FormGroup;
  @Input() array: FormArray;
  @Input() arrayName: string;

  @Input() controlName: string;
  @Input() controlType: ControlType;
  @Input() ddlOptions: IdName[];
  @Input() valuePropName: string;
  @Input() namePropName: string;

  @Input() wide: boolean;

  @Input() type: TextboxType;
  @Input() tooltipText: string;
  @Input() attentionText: string;
  @Input() showBigRequiredSign: boolean;

  @Output() onSearch = new EventEmitter<string>();

  controlTypeEnum = ControlType;
  constructor() { }

  ngOnInit() {
  }

  get field(): AbstractControl {
    if (this.group !== undefined) {
      return this.group.get(this.controlName);
    }
    return null;
  }

  public searchItems(value: string) {
    this.onSearch.emit(value);
  }
}
