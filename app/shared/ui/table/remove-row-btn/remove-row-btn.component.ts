import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-remove-row-btn',
  templateUrl: './remove-row-btn.component.html',
  styleUrls: ['./remove-row-btn.component.scss']
})
export class RemoveRowBtnComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() disabled: boolean;
  @Input() disabledTooltip: string;
  @Output() onClick = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  click(event) {
    this.onClick.emit();
  }
}
