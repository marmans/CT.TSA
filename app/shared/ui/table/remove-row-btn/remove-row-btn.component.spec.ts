import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveRowBtnComponent } from './remove-row-btn.component';

describe('RemoveRowBtnComponent', () => {
  let component: RemoveRowBtnComponent;
  let fixture: ComponentFixture<RemoveRowBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveRowBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveRowBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
