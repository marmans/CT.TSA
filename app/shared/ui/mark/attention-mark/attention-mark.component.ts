import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-attention-mark',
  templateUrl: './attention-mark.component.html',
  styleUrls: ['./attention-mark.component.scss']
})
export class AttentionMarkComponent implements OnInit {
  @Input() tooltipText: string;
  @Input() field: AbstractControl;

  defaultText = 'This field is required';

  constructor() { }

  ngOnInit() {
  }

}
