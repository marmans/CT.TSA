import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentionMarkComponent } from './attention-mark.component';

describe('AttentionMarkComponent', () => {
  let component: AttentionMarkComponent;
  let fixture: ComponentFixture<AttentionMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttentionMarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentionMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
