import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudButtonBarComponent } from './crud-button-bar.component';

describe('CrudButtonBarComponent', () => {
  let component: CrudButtonBarComponent;
  let fixture: ComponentFixture<CrudButtonBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudButtonBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
