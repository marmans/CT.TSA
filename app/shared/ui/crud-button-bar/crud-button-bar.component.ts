import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-crud-button-bar',
  templateUrl: './crud-button-bar.component.html',
  styleUrls: ['./crud-button-bar.component.scss']
})
export class CrudButtonBarComponent implements OnInit {
  @Input() editMode: boolean;
  @Input() hideEdit: boolean;
  @Input() hideSave: boolean;
  @Input() hideCancel: boolean;
  @Input() hideRemove: boolean;
  @Input() formIsValid: boolean;

  @Input() disabledRemove: boolean;
  @Input() disabledRemoveTooltip: string;

  @Output() onEditClicked = new EventEmitter<boolean>();
  @Output() onCancelClicked = new EventEmitter<boolean>();
  @Output() onSaveClicked = new EventEmitter<boolean>();
  @Output() onRemoveClicked = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  editClicked() {
    this.onEditClicked.emit(true);
  }
  cancelClicked() {
    this.onCancelClicked.emit(true);
  }
  saveClicked() {
    this.onSaveClicked.emit(true);
  }
  removeClicked() {
    this.onRemoveClicked.emit(true);
  }
}
