import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.scss']
})
export class ModalDialogComponent implements OnInit {
  @Input() show: boolean;
  @Input() title: string;
  @Output() onHide = new EventEmitter<boolean>();
  @Input() styleOptions;
  constructor() {
  }

  ngOnInit() {
  }

  hideModal() {
    this.show = false;
    this.onHide.emit(true);
  }
}
