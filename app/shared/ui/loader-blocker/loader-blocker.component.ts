import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loader-blocker',
  templateUrl: './loader-blocker.component.html',
  styleUrls: ['./loader-blocker.component.scss']
})
export class LoaderBlockerComponent implements OnInit {
  @Input() loading: boolean;
  constructor() { }

  ngOnInit() {
  }

}
