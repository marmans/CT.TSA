import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderBlockerComponent } from './loader-blocker.component';

describe('LoaderBlockerComponent', () => {
  let component: LoaderBlockerComponent;
  let fixture: ComponentFixture<LoaderBlockerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderBlockerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderBlockerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
