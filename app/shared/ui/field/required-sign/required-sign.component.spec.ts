import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredSignComponent } from './required-sign.component';

describe('RequiredSignComponent', () => {
  let component: RequiredSignComponent;
  let fixture: ComponentFixture<RequiredSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
