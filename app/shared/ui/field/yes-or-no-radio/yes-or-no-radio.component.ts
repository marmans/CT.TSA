import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';

@Component({
  selector: 'app-yes-or-no-radio',
  templateUrl: './yes-or-no-radio.component.html',
  styleUrls: ['./yes-or-no-radio.component.scss']
})
export class YesOrNoRadioComponent extends BasicField  implements OnInit {
  @Input() editMode: boolean;
  @Input() group: FormGroup;
  @Input() controlName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
