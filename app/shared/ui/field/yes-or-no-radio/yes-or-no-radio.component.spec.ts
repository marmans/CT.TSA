import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YesOrNoRadioComponent } from './yes-or-no-radio.component';

describe('YesOrNoRadioComponent', () => {
  let component: YesOrNoRadioComponent;
  let fixture: ComponentFixture<YesOrNoRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YesOrNoRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YesOrNoRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
