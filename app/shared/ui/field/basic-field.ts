import { Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

export class BasicField {
  @Input() group: FormGroup;
  @Input() controlName: string;
  @Input() arrayName: string;

  getFieldValue() {
    const s = this.group.get(this.controlName);
    return s ? s.value : null;
  }

  getFormArrayByName() {
    return this.group.get(this.arrayName) as FormArray;
  }
}
