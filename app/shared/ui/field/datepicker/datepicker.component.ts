import { Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges, forwardRef, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import { BasicField } from '../basic-field';
import { Utils } from '@app/services/utils';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DatepickerComponent),
    multi: true
  }]
})
export class DatepickerComponent implements ControlValueAccessor, OnInit {
// extends BasicField implements OnInit {
  myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    showClearDateBtn: false,
  };
  @Input() editMode: boolean;
  @Input() required: boolean;
  @Input() showTime: boolean;

  fieldForm: FormGroup;
  onChange: Function;

  // call this for all user actions with controll
  onTouched: Function;
  private _value: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.fieldForm = this.fb.group({
      field: []
    });
    if (this.editMode) {
      this.fieldForm.valueChanges.subscribe(res => {
        this.value = Utils.toJsDateString(res.field.date);
      });
    }
  }

  convertValueToShow() {
    if (this.value) {
      const formValue = { date: (this.value === null ? {} : Utils.toIMyDate(this.value) ) };
      this.fieldForm.patchValue({field: formValue});
    }
  }

  get value() {
    return this._value;
  }

  // set new value and inform form about that
  set value(value) {
    this._value = value;
    if (this.onChange) {
      this.onChange(value);
    }
  }

  // call this if form was changed outside
  writeValue(obj: any): void {
    this.value = obj;
    this.convertValueToShow();
  }

  // save callback for form changes
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // save callback for touch events
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  getDateString() {
    const value = this.fieldForm.get('field').value;
    if (value !== null) {
      const date = value.date;
      const m = Utils.LeadingZero(date.month);
      const d = Utils.LeadingZero(date.day);
      const y = date.year;

      return `${d}.${m}.${y}`;
    }
  }
}
