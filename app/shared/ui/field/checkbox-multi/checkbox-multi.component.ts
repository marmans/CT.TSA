
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BasicField } from '../basic-field';

@Component({
  selector: 'app-checkbox-multi',
  templateUrl: './checkbox-multi.component.html',
  styleUrls: ['./checkbox-multi.component.scss']
})
export class CheckboxMultiComponent extends BasicField implements OnInit, AfterViewInit, OnChanges {
  @Input() editMode: boolean;
  @Input() required: boolean;

  @Input() group: FormGroup;
  @Input() controlName: string;
  @Input() options: any[];
  @Input() valuePropName: string;
  @Input() namePropName: string;

  fieldForm: FormGroup;
  controlNameValue;
  groupValue;
  readOnlySelectedItems: any[] = [];

  constructor(
    private fb: FormBuilder) {
    super();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.initAll();
  }

  ngOnInit() {
    this.initFieldForm();
  }

  ngAfterViewInit(): void {
    this.initAll();
  }

  initAll() {
    if (this.controlName && this.group && this.options.length > 0) {
      const selectedValues = this.createSelectedValues();
      this.initFieldForm();
      this.fieldForm.patchValue({ field: selectedValues });
      this.updateOutputValue();
      this.readOnlySelectedItems = this.getSelectedItemsNames();
    }
  }

  initFieldForm() {
    this.fieldForm = this.fb.group({
      field: this.fb.array(this.createArrayFromOptions())
    });
  }

  get fieldArr() {
    return this.fieldForm.get('field');
  }

  createArrayFromOptions() {
    const fa = this.options.map(el => {
      return this.fb.control(false);
    });
    return fa;
  }
  createSelectedValues() {
    // here we use array of selected values
    const selected: any[] = this.getFieldValue();
    if (selected) {
      return this.options.map(el => {
        if (selected.indexOf(el[this.valuePropName]) > -1) {
          return true;
        }
        return false;
      });
    } else {
      return this.options.map(el => {
        return false;
      });
    }
  }
  convertSelectedValuesToOutput(values) {
    const arr: boolean[] = values.field;
    const resultArr = [];
    arr.forEach((el, index) => {
      const curr = this.options[index];
      if (el) {
        return resultArr.push(curr[this.valuePropName]);
      }
    });
    return resultArr;
  }
  updateOutputValue() {
    this.fieldForm.valueChanges.subscribe(res => {
      const resultArr = this.convertSelectedValuesToOutput(res);
      this.group.get(this.controlName).patchValue(resultArr);
    });
  }

  getSelectedItemsNames() {
    const selected: any[] = this.getFieldValue();
    if (selected) {
      const selectedItems: any[] = [];
      this.options.forEach(el => {
        if (selected.indexOf(el[this.valuePropName]) > -1) {
          selectedItems.push(el);
        }
      });
      return selectedItems;
    }
  }
}
