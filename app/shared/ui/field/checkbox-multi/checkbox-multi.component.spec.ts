import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxMultiComponent } from './checkbox-multi.component';

describe('CheckboxMultiComponent', () => {
  let component: CheckboxMultiComponent;
  let fixture: ComponentFixture<CheckboxMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
