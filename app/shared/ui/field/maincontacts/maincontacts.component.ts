import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';

@Component({
  selector: 'app-maincontacts',
  templateUrl: './maincontacts.component.html',
  styleUrls: ['./maincontacts.component.scss']
})
export class MaincontactsComponent extends BasicField  implements OnInit {
  @Input() editMode: boolean;
  @Input() required: boolean;

  @Input() group: FormGroup;
  @Input() controlName: string;
  constructor() {
    super();
  }

  ngOnInit() { }
}
