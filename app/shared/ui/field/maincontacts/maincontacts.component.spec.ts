import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaincontactsComponent } from './maincontacts.component';

describe('MaincontactsComponent', () => {
  let component: MaincontactsComponent;
  let fixture: ComponentFixture<MaincontactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaincontactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaincontactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
