import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent extends BasicField implements OnInit {
  @Input() editMode: boolean;
  @Input() required: boolean;

  @Input() group: FormGroup;
  @Input() controlName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
