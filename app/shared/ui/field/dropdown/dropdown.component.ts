import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IdName } from '@app/models/idName';
import { FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent  extends BasicField implements OnInit {

  selectedValueValue: IdName;
  @Input() options: any[];
  @Input() editMode: boolean;
  @Input() required: boolean;

  @Output() selectedValueChange = new EventEmitter<IdName>();

  @Input() group: FormGroup;
  @Input() controlName: string;
  @Input() includeEmpty = true;

  @Input() valuePropName: string;
  @Input() namePropName: string;
  constructor() {
    super();
  }

  ngOnInit() {
  }

  compareFn(a: IdName, b: IdName) {
    return a && b && a.Id === b.Id;
  }

  @Input()
  get selectedValue(){
    return this.selectedValueValue;
  }

  set selectedValue(val) {
    this.selectedValueValue = val;
    this.selectedValueChange.emit(this.selectedValueValue);
  }

  get currentReadonlyValue(): string {
    const currentValue = this.getFieldValue();
    if (this.options !== undefined && this.options.length > 0) {
      const currentObject = this.options.find(x => x[this.valuePropName] === currentValue);
      if (currentObject) {
        return currentObject[this.namePropName];
      }
    }
    return '';
  }
}
