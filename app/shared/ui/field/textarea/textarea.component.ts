import { Component, OnInit, Input } from '@angular/core';
import { BasicField } from '../basic-field';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent extends BasicField implements OnInit {
  @Input() editMode: boolean;
  @Input() required: boolean;

  @Input() group: FormGroup;
  @Input() controlName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
