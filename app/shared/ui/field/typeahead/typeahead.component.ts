import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ValueLabel } from '@app/models/ui';

@Component({
  selector: 'app-typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss']
})
export class TypeaheadComponent extends BasicField implements OnInit {
  private debouncingValue = new Subject<string>();

  @Input() editMode: boolean;
  @Input() group: FormGroup;
  @Input() controlName: string;
  @Input() items: ValueLabel[] = [];
  @Input() debounceMs = 500;
  @Input() selectedItemId: number;
  @Input() multiple: boolean;

  @Output() onSelectItem = new EventEmitter<any>();
  @Output() onSearch = new EventEmitter<string>();

  constructor() {
    super();
  }

  ngOnInit() {
    this.debouncedSearch();
  }

  searchItems(value) {
    this.debouncingValue.next(value['term']);
  }

  openSearch(value) {
    // console.log(value, this.debouncingValue.);
  }
  changeValue(value) {
    if (value) {
      this.updateSelectedItem(value.value);
    }
  }

  get currentReadonlyValue(): string {
    const currentId = this.getFieldValue();
    if (this.items !== undefined && this.items.length > 0) {
      const item = this.items.find(x => (x as ValueLabel).value === currentId);
      if (item) {
        return item.label;
      }
    }
    return currentId;
  }

  debouncedSearch() {
    this.debouncingValue.pipe(debounceTime(this.debounceMs), distinctUntilChanged())
    .subscribe(res => {
      this.onSearch.emit(res);
    });
  }

  public updateSelectedItem(value: number) {
    if (value !== undefined) {
      this.selectedItemId = value;
      this.onSelectItem.emit(this.selectedItemId);
    } else {
      if (this.selectedItemId !== undefined) {
        this.onSelectItem.emit(this.selectedItemId);
      }
    }
  }
}
