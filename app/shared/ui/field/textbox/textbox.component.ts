import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BasicField } from '../basic-field';
import { TextboxType } from '@app/models/ui';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent extends BasicField implements OnInit {
  @Input() editMode: boolean;
  @Input() required: boolean;
  @Input() type: TextboxType;

  @Input() group: FormGroup;
  @Input() controlName: string;

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getTypeByTextboxType() {
    switch (this.type) {
      case TextboxType.Number:
        return 'number';
      case TextboxType.Text:
        return 'text';
      case TextboxType.Email:
        return 'email';
      default:
        return 'text';
    }
    return '';
  }
}
