export class BasicComponent {
  // use for forms like ``` <form (keydown.enter)="handleEnterKeyPress($event)" > ... ```
  public handleEnterKeyPress(event) {
    const tagName = event.target.tagName.toLowerCase();
    if (tagName !== 'textarea') {
      return false;
    }
  }
}
