import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PendingsiteRoutingModule } from './pendingsite-routing.module';
import { PendingsiteListComponent } from './pendingsite/pendingsite-list/pendingsite-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PendingsiteFilterComponent } from './pendingsite/pendingsite-filter/pendingsite-filter.component';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  declarations: [
    PendingsiteListComponent,
    PendingsiteFilterComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PendingsiteRoutingModule,
    NgxPaginationModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class PendingsiteModule { }
