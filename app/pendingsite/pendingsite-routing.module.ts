import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingsiteListComponent } from './pendingsite/pendingsite-list/pendingsite-list.component';

const routes: Routes = [
  {
    path: '',
    component: PendingsiteListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingsiteRoutingModule { }
