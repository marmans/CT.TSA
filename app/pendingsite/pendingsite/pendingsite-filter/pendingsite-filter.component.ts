import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NameValue, ValueLabel } from '@app/models/ui';
import { PendingSiteFilter, AddressTypeEnum } from '@app/models/site';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-pendingsite-filter',
  templateUrl: './pendingsite-filter.component.html',
  styleUrls: ['./pendingsite-filter.component.scss']
})
export class PendingsiteFilterComponent implements OnInit {
  filterForm: FormGroup;
  pageSizeOptions: NameValue[] = [];
  typeItems: ValueLabel[] = [];

  @Input() filter: PendingSiteFilter;
  @Output() filterChanged = new EventEmitter<PendingSiteFilter>();
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.getSiteTypes();
    this.initForm();
    this.onChange();
  }

  initForm() {
    this.pageSizeOptions = [{ Name: '20', Value: '20' }, { Name: '50', Value: '50' }];
    this.filterForm = this.fb.group({
      AddressTypes: [],
      WaitingForApprovalSince: [],
      Page: [],
      PageSize: ['20'],
    });
    this.filterForm.patchValue(this.filter);
  }

  onChange() {
    this.filterForm.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(res => {
        this.filter = this.filterForm.value;
        this.filterChanged.emit(this.filter);
      });
  }
  getSiteTypes() {
    this.typeItems = [
      { label: 'Packing', value: AddressTypeEnum.Packing },
      { label: 'Production', value: AddressTypeEnum.Production },
      { label: 'Warehouse', value: AddressTypeEnum.Warehouse },
      { label: 'Pickup', value: AddressTypeEnum.Pickup },
    ];
  }
}
