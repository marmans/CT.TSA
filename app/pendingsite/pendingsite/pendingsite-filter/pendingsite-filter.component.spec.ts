import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingsiteFilterComponent } from './pendingsite-filter.component';

describe('PendingsiteFilterComponent', () => {
  let component: PendingsiteFilterComponent;
  let fixture: ComponentFixture<PendingsiteFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingsiteFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingsiteFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
