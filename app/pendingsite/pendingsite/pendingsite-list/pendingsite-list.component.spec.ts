import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingsiteListComponent } from './pendingsite-list.component';

describe('PendingsiteListComponent', () => {
  let component: PendingsiteListComponent;
  let fixture: ComponentFixture<PendingsiteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingsiteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingsiteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
