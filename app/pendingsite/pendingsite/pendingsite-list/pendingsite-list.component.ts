import { Component, OnInit } from '@angular/core';
import { Address, SiteListFilter, PendingSiteFilter, AddressTypeEnum } from '@app/models/site';
import { SiteService } from '@app/services/site.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { FilterForListsService } from '@app/services/filter-for-lists.service';
import { ValueLabel } from '@app/models/ui';

@Component({
  selector: 'app-pendingsite-list',
  templateUrl: './pendingsite-list.component.html',
  styleUrls: ['./pendingsite-list.component.scss']
})
export class PendingsiteListComponent implements OnInit {
  items: Address[] = [];
  loading: boolean;
  filterParams: PendingSiteFilter = {
    Page: 1,
    PageSize: 20,
    AddressTypes: []
  };
  total: number;
  subscriptions = new Subscription();
  typeItems: ValueLabel[] = [];

  defaultFilterParams: PendingSiteFilter = {
    PageSize: 20,
    Page: 1,
    AddressTypes: [
      AddressTypeEnum.Packing,
      AddressTypeEnum.Pickup,
      AddressTypeEnum.Production,
      AddressTypeEnum.Warehouse
    ],
    WaitingForApprovalSince: new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 2,
      new Date().getDate()
    ).toISOString()
  };

  constructor(private siteService: SiteService,
    private route: ActivatedRoute,
    private filterForListsService: FilterForListsService,
    private router: Router
  ) {
    this.filterForListsService.init<PendingSiteFilter>(this.defaultFilterParams);
  }

  ngOnInit() {
    this.bindFilterParamsToRouteData();
  }

  private bindFilterParamsToRouteData() {
    this.subscriptions.add(this.route.paramMap
      .subscribe((paramMap: ParamMap) => {
        this.filterForListsService.getFilterParamsFromRouteParamMap<PendingSiteFilter>(paramMap).subscribe(filterParams => {
          this.filterParams = filterParams;
          this.getPendingSites();
        });
      }));
  }

  getPendingSites(): void {
    this.loading = true;
    this.siteService.getPendingSiteList(this.filterParams)
      .subscribe(items => {
        this.items = items.result.data;
        this.total = items.result.TotalCount;
      },
        err => {
          console.log(err);
        },
        () => {
          this.loading = false;
        }
      );
  }

  pageChanged(page: number) {
    this.filterParams.Page = page;
    this.getPendingSites();
  }

  filterChanged(filter: PendingSiteFilter) {
    this.filterParams = filter;
    this.subscriptions.add(this.filterForListsService.getRouteParamsFromFilterParams<PendingSiteFilter>(this.filterParams)
      .subscribe((urlParams: any) => {
        this.router.navigate(['/pendingsite', urlParams]);
      }, err => {
        console.log(err);
      }));
  }
}
