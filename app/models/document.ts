export class DocunoteDocument {
  DocumentId: string;
  DocumentVersionId: string;
  Name: string;
  Url: string;
  Status: DocumentStatus;
  ContactPersonName: string;
  SignDate?: Date;
  Selected: boolean;
}

export enum DocumentStatus {
  ToBeSigned = 0,
  Signed = 1
}
