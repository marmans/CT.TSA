export class Message {
    type: MessageType;
    text: string;
}

export enum MessageType {
    Success,
    Error,
    Info,
    Warning
}
