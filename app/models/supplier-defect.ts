export class SupplierDefectListItem {
  CaseNumber: number;
  CreatedByText: string;
  DefectCategoryName: string;
  DefectFollowUpName: string;
  DefectReceivedSourceName: string;
  Id: number;
  ReportDate;
  ReportedSourceName: string;
}

export class SupplierDefect extends SupplierDefectListItem {
  Id: number;
  SupplierId: number;
  ReportedSource: number; // ReportedBy;
  ReportDate; // Date;
  DefectReceivedSource: number; // ReceivedFrom;
  CreatedByText: string; // CreatedBy;
  DefectCategory: number; // CategoryOfDefect;
  DefectFollowUp: number; // Action;
  DefectClosingRemark?: number; // ClosingRemark;
  SupplierDefectProducts: SupplierDefectProduct[] = [];
  ShortTermActions: string;
  Comments: string; // TheCause
  ReferenceArchive: string;
}

export class SupplierDefectFilter {
  SupplierId?: number;
  Page?: number;
  PageSize: number;
}
class DdlOptionItem {
  Id: number;
  Name: string;
  SortOrder: number;
  Active: boolean;
}
export class DefectReportedSource extends DdlOptionItem {
}
export class DefectReceivedSource extends DdlOptionItem {
}
export class DefectCategory extends DdlOptionItem {
}
export class DefectFollowUp extends DdlOptionItem {
}
export class DefectClosingRemark extends DdlOptionItem {
}

export class SupplierDefectProduct {
  Id: number;
  Gtin: number;
  ProductText: string;
  Brand: string;
  CategoryName: string;
  MarketNames: string[];
  LotNo: string;
  PackagingDate: string;
  BestBefore: string;
  isDk?: boolean;
  isNo?: boolean;
  isSe?: boolean;
  isFi?: boolean;
}
