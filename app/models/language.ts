export class Language {
    Id: number;
    Name: string;
    IsoCode: string;
}
