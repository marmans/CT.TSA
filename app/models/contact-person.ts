import { IdName } from './idName';

export interface ContactPersonListItem {
    Id: number;
    PersonSalutationId: PersonSalutation;
    FirstName: string;
    LastName: string;
    PersonTypes: IdName;
    Email: string;
    PhoneNumberText: string;
    LoginName: string;
    IsDisabled: boolean;
}

export class ContactPersonFilter {
    PageSize = 20;
    Page = 1;
    SupplierId: number;
    ShowActive = true;
    ShowPersonsWithLoginId = true;
}

export class ContactPerson {
    Id: number;
    PersonTypeIds: number[];
    PersonSalutationId: number;
    TitleName: string;
    FirstName: string;
    LastName: string;
    PhoneNumberText: string;
    MobileNumberText: string;
    FaxNumberText: string;
    Email: string;
    PrimaryLanguageCode: string;
    ModifiedDate: Date;
    ModifiedByText: string;
    LoginName: string;
    IsDisabled: boolean;
}

export enum PersonSalutation {
    Mr = 1,
    Mrs = 2,
    Ms = 3
}
