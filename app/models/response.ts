export class ValidationResult {
  IsValid: boolean;
  ValidationErrors: ValidationError[] = [];
}

export class ValidationError {
  PropertyName: string;
  ErrorMessage: string;
}

export class SaveResult {
  ValidationResult: ValidationResult;
  Id: number;
}
