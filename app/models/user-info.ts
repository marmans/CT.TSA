import { UserRole } from './user-role';

export class UserInfo {
    /** user name (login without domain part) - mandatory for trading users */
    UserName: string;

    /** supplierId - only for supplier users */
    SupplierId?: number;

    /** person id - only for one time login users */
    PersonId?: number;

    /** user role */
    Role: UserRole;

    get isTradingUser(): boolean {
        return this.Role === UserRole.TradingUser;
    }

    constructor(userName = '', supplierId?: number, role = UserRole.Restricted) {
        this.UserName = userName;
        this.SupplierId = supplierId;
        this.Role = role;
    }
}

export class OneTimeLoginStatusDto {
    Id: number;
    IsPersonOneTime?: boolean;
    IsSupplierOneTime?: boolean;
}
