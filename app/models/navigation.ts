import { UserRole } from './user-role';

export interface NavItem {
    Id?: number;
    Name: string;
    Path: string;
    Children?: NavItem[];
    Roles?: UserRole[];
    IsSeparator?: boolean;
}
