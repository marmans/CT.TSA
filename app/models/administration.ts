import { PaymentTerm } from './supplier';

export class CreateOneTimeLoginDto {
    Email: string;
    CompanyName: string;
    PaymentTerms: PaymentTerm[] = [];
}
