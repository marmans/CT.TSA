export class Country {
    Id: number;
    Name: string;
    IsoCode: string;
}

export class CountryPort {
    Id: number;
    CountryId: number;
    PortName: string;
    Country: Country;
}
