import { IdName } from './idName';
import { Address } from './site';
import { Country } from './country';

export class SupplierListItem {
    Id: number;
    Name: string;
    MainContact: string;
    VAT: string;
    Status: string;
}

export class Supplier {
    Id: number;
    Name: string;
    VAT: string;
    PrimaryPostalAddress: Address;
    PrimaryVisitingAddress: Address;
    CountryIsoCode: string;
    GlnNumber: string;
    Url: string; // Home page
    IsAgent?: boolean; // may be true, false and null
    IsPackagingMaterialSupplier?: boolean; // ?
    CanSendEdiMessageBol: boolean;
    IsActiveFaV: boolean;
    IsIndirectGaS: boolean;
    Countries: Country[];
    DisabledDate?: Date;
    DisabledRemark: string;
    MainContact: string;
    Status: string;
    PaymentTerms: PaymentTerm[] = [];
}

export class SupplierListFilter {
    SupplierName?: string;
    VAT?: string;
    Statuses: SupplierFilterStatus[];
    Page?: number;
    PageSize: number;
}

export enum SupplierFilterStatus {
    Approved = 3,
    Previous = 5,
    Blocked = 7,
    Active = 8,
    Pending = 9
}

export interface PaymentTerm {
    NetDays: string;
    CashDiscount: string;
    Comment: string;
    MarketId: number;
    MarketName: string;
    SupplierCompanyId: number;
}
