export enum ControlType {
    Autocomplete = 100,
    Dropdown = 200,
    MainContacts = 250,
    Textbox = 300,
    YesOrNoRadio = 400,
    CheckboxMulti = 500,
    Checkbox = 600,
    Textarea = 700,
    Date = 800
}

export enum TextboxType {
    Text,
    Number,
    Email
}

export interface Country {
    IsoCode: string;
    Name: string;
}

export interface NameValue {
    Name: string;
    Value: string;
}

export interface ValueLabel {
    value: any;
    label: string;
    disabled?: boolean;
}
