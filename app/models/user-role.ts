export enum UserRole {
    Restricted = 1,
    TradingUser = 10,
    Supplier = 20,
    Translator = 30,
    ExternalUser = 40,
    SupplierSign = 50,
    OneTimeLogin = 60
}
