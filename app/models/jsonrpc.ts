export interface Result<T> {
    result: T;
    id: number;
    jsonrpc: string;
    error: Error<T>;
}

export interface Pageable<T> {
    data: T;
    TotalCount: number;
}

export interface Error<T> {
    code: number;
    data?: T;
    message: string;
}
