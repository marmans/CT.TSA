import { IMyDate } from 'mydatepicker';
import { IdName } from './idName';

export interface Address {
    Id: number;
    CompanyId?: number;
    AddressStatusId: number;
    AddressTypes: IdName[];
    Name: string;
    AddressLine1Text: string;
    AddressLine2Text: string;
    AddressLine3Text: string;
    PostBoxTerm: string;
    ZipCodeTerm: string;
    CityName: string;
    CountryIsoCode: string;
    CountryPortId?: number;
    PhoneNumberText: string;
    MobileNumberText: string;
    FaxNumberText: string;
    AuthorityApprovalNumberText: string;
    EmergencyNumberText: string;
    ModifiedDate: Date;
    ModifiedByText: string;
    CreatedDate: Date;
    CreatedByText: string;
    IsUsedInPpl: boolean;
    IsUsedInSpecifications: boolean;
}

export class SiteListFilter {
    SupplierId?: number;
    Page = 1;
    PageSize = '20';
    ShowActiveOnly = true;
}

export class PendingSiteFilter {
    Page?: number;
    PageSize: number;
    AddressTypes: AddressTypeEnum[] = [];
    WaitingForApprovalSince?;
}

export enum AddressTypeEnum {
    Postal = 1,
    Packing = 5,
    Production = 6,
    Warehouse = 9,
    Pickup = 10
}
export enum AddressStatus {
    Approved = 1,
    ApprovedWithoutAudit = 2,
    ApprovedWillPerformAudit = 3,
    Rejected = 4,
    AwaitingApproval = 5,
    ApprovedBySystem = 6,
    Disabled = 7
}
