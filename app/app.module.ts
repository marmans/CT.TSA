import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { SupplierService } from './services/supplier.service';
import { TestModeConfigurationService } from './services/test-mode-configuration.service';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule } from './shared/shared.module';
import { NavigationService } from './services/navigation.service';
import { SiteService } from './services/site.service';
import { TestModeConfigurationComponent } from './test-mode-configuration/test-mode-configuration.component';
import { AuthorizationService } from './services/authorization.service';
import { MyDatePickerModule } from 'mydatepicker';
import { FilterForListsService } from './services/filter-for-lists.service';
import { ErrorInterceptor } from './http-interceptors/error.interceptor';
import { MessageComponent } from './message/message.component';
import { MessageService } from './services/message.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    TestModeConfigurationComponent,
    MessageComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    NgSelectModule,
  ],
  providers: [
    SupplierService,
    NavigationService,
    SiteService,
    AuthorizationService,
    TestModeConfigurationService,
    FilterForListsService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
