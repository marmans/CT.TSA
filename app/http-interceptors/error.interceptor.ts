import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { MessageService } from '@app/services/message.service';
import { map, finalize } from 'rxjs/operators';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor (public messageService: MessageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          const body = event.body;
          if (body !== undefined) {
            const props = Object.getOwnPropertyNames(body);
            if (props !== null || props !== undefined) {
              if (props.filter(prop => prop === 'error').length > 0) {
                this.messageService.error(body['error']['message']);
              }
            }
          }
          return event;
        }
      })
    );
  }
}
